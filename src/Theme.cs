//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2005 Imendio AB
// (C) 2005 Michael Ostermeier

using System.IO;

namespace Imendio.Blam {
	public class Theme {
		private string mName = "";
		private string mPath = "";

		private string mThemeHtml = "";

		public Theme (string path)
		{
			mPath = path;

            Load ();
		}

		public string Name {
			get {
				return mName;
			}
		}

		public string Path {
			get {
				return mPath;
			}
		}

		public string Render(string[] args)
		{
		    int off = 0;
		    int max = args.Length;
		    string str = mThemeHtml;

		   /*
		    * We take pairs of the keyword to replace and the text
		    * to replace it with.
		    */

		    for(; off + 1 < max; off += 2) {
		        str = str.Replace("${" + args[off] + "}", args[off + 1]);
		    }

		    return str;
		}

		private void Load () 
		{
            string fileName = mPath + "/" + Defines.THEME_INDEX_NAME;
            TextReader r = File.OpenText (fileName);
            mThemeHtml = r.ReadToEnd ();
            r.Close ();

            // TODO: Add better error handling here
            mName = mPath.Substring (mPath.LastIndexOf ("/") + 1);
		}
	}
}
