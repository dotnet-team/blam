//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using Gdk;
using Gtk;
using GtkSharp;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using Mono.Unix;

namespace Imendio.Blam {

    public class ChannelList : Gtk.TreeView {

        // Events
        public event ChannelEventHandler ChannelSelectedEvent;

        // Emitted when using right-click popup
        public event ChannelEventHandler RemoveChannelEvent;
        public event ChannelEventHandler MarkChannelAsReadEvent;
        public event ChannelEventHandler EditChannelEvent;
        public event ChannelEventHandler RefreshChannelEvent;

        private EventHandler selectionChangedHandler;

        private ChannelMenu popupMenu;

        private TreeViewColumn nameColumn;

        private IEnumerator channelEnumerator;
        private IEnumerator groupEnumerator;

        private IChannel LastChannel = null;

        public static TargetEntry[] DragEntries = new TargetEntry[] {
            new TargetEntry("channel", TargetFlags.Widget, (uint)TargetType.Channel)
       };

        public ChannelList(IList channels, IList groups)
        { 
            TreeViewColumn col;
            CellRenderer   cell;

            /* Channel name column */
            nameColumn = new TreeViewColumn();
            cell = new CellRendererText();

            nameColumn.PackStart(cell, true);
            nameColumn.Sizing = TreeViewColumnSizing.GrowOnly;
            nameColumn.Expand = true;
            nameColumn.SetCellDataFunc(cell, new TreeCellDataFunc(NamesCellDataFunc));

            AppendColumn(nameColumn);

            /* Items column */
            col = new TreeViewColumn();
            cell = new CellRendererText();

            col.PackStart(cell, true);
            col.SetCellDataFunc(cell,
                                new TreeCellDataFunc(ItemsCellDataFunc));

            AppendColumn(col);

            this.RulesHint = true;

            selectionChangedHandler = new EventHandler(SelectionChanged);
            this.Selection.Changed += selectionChangedHandler;

            this.Model = new TreeStore (typeof(Channel));
            this.HeadersVisible = false;

            // Sort the list
            (Model as TreeStore).DefaultSortFunc = ChannelSort;
            (this.Model as TreeStore).SetSortColumnId (-1, SortType.Ascending);

            // Right click popup
            this.popupMenu = new ChannelMenu();
            this.popupMenu.EditSelected       += EditChannelCb;
            this.popupMenu.MarkAsReadSelected += MarkAsReadCb;
            this.popupMenu.RemoveSelected     += RemoveChannelCb;
            this.popupMenu.RefreshSelected    += RefreshChannelCb;

            channelEnumerator = channels.GetEnumerator();
            groupEnumerator = groups.GetEnumerator();

			EnableModelDragSource(ModifierType.Button1Mask, DragEntries, DragAction.Copy);
			DragDataGet += DragDataGetHandler;
			EnableModelDragDest(DragEntries, DragAction.Copy);
			DragDataReceived += DragDataReceivedHandler;

            GLib.Idle.Add(new GLib.IdleHandler(IdleAdd));
        }

        private void ForceResort()
        {
            (Model as TreeStore).SetSortColumnId(-1, SortType.Descending);
            (Model as TreeStore).SetSortColumnId(-1, SortType.Ascending);
        }

		private void DragDataGetHandler(object o, DragDataGetArgs args)
		{
			ChannelList chlst = o as ChannelList;
			args.RetVal = chlst.GetSelected();

		}

		private void DragDataReceivedHandler(object o, DragDataReceivedArgs args)
		{
			TreePath path;
			TreeViewDropPosition pos;
			TreeIter iter, tmp_iter;
			IChannel src = GetSelected();
			tmp_iter = src.Iter;
			GetDestRowAtPos(args.X, args.Y, out path, out pos);
			Model.GetIter(out iter, path);
			IChannel dst = Model.GetValue(iter, 0) as IChannel;

			/* Not directly over a channel. */
			if(pos == TreeViewDropPosition.After || pos == TreeViewDropPosition.Before){

				TreePath tmp_path = path.Copy();
				tmp_path.Up();
				TreePath tmp_path_src = Model.GetPath(src.Iter).Copy();
				tmp_path_src.Up();
				if(tmp_path == tmp_path_src){ // Same parent.
					System.Console.WriteLine("return!");
					return;
				}

				Blam.Application.TheApp.CCollection.Remove(src);

				Model.GetIter(out tmp_iter, tmp_path);
				IChannel drop_parent = Model.GetValue(tmp_iter, 0) as IChannel;
				//System.Console.WriteLine("dp: {0} depth:{1}", drop_parent.GetType(), path.Depth);
				if(path.Depth == 2 && drop_parent is ChannelGroup){ // Dropped inside a group.
					Blam.Application.TheApp.CCollection.Add(drop_parent as ChannelGroup, src);
				} else{
					Blam.Application.TheApp.CCollection.Add(src);
				}
			/* Directly over a channel. */
			} else {
				TreePath tmp_path_src = Model.GetPath(src.Iter).Copy();
				tmp_path_src.Up();
				TreePath dst_path_parent = path.Copy();
				dst_path_parent.Up();

				if(dst is Channel && path.Depth == 1 && // dst is top-level
				   src is Channel && Model.GetPath(src.Iter).Depth == 2){ // src is in a group
					Blam.Application.TheApp.CCollection.Remove(src);
					Blam.Application.TheApp.CCollection.Add(src);
				}

				/*
				 * A channel can't become a group and a group can't be
				 * inside another group and it doesn't make sense to move
				 * it into the same group.
				 * FIXME: Make it possible for a group to be inside another group.
				 */
				if((dst is Channel && path.Depth == 1) || src is ChannelGroup ||
				   path == tmp_path_src){ // Move to the same group
					return;
				}

				Blam.Application.TheApp.CCollection.Remove(src);
				/*
				 * If we drop on a channel which is inside a group, we add that channel to
				 * the group.
				 */

				if(dst is Channel && path.Depth == 2){ // Channel inside a group.
					Model.GetIter(out tmp_iter, dst_path_parent);
					dst = Model.GetValue(tmp_iter, 0) as ChannelGroup;
				}

				Blam.Application.TheApp.CCollection.Add(dst as ChannelGroup, src);
			}
		}

        private bool IdleAdd()
        {
            while (channelEnumerator.MoveNext ()) {
                IChannel channel = channelEnumerator.Current as IChannel;
                Add(channel);
            }

            while(groupEnumerator.MoveNext()){
                IChannel group = groupEnumerator.Current as IChannel;
                AddGroup(group);
            }

            return false;
        }

        public void Add (IChannel channel)
        {
            channel.Iter = (this.Model as TreeStore).AppendValues(channel);
        }

		public void AddToGroup(IChannel group, IChannel channel)
		{
			channel.Iter = (Model as TreeStore).AppendValues(group.Iter, channel);
		}

        public void AddGroup(IChannel channel)
        {
            TreeIter iter = (this.Model as TreeStore).AppendValues(channel);
            ChannelGroup group = channel as ChannelGroup;
            group.Iter = iter;
			SetDragDestRow(Model.GetPath(iter), TreeViewDropPosition.IntoOrAfter);

            foreach(IChannel chan in group.Channels){
                iter = (this.Model as TreeStore).AppendValues(group.Iter, chan);
                chan.Iter = iter;
            }
        }

        public IChannel GetSelected ()
        {
            TreeModel model;
            TreeIter  iter;

            if (!this.Selection.GetSelected(out model, out iter)) {
                return null;
            }

            return(IChannel)model.GetValue(iter, 0);
        }

        public void Updated (IChannel channel)
        {
            TreeIter iter = FindChannel (channel);

            if (!iter.Equals(TreeIter.Zero)) {
                this.Model.EmitRowChanged (this.Model.GetPath(iter), iter);
            }

            ForceResort();

        }

        public void Remove (IChannel channel)
        {
            TreeIter iter = FindChannel(channel);
            bool valid;

            if (!iter.Equals(TreeIter.Zero)) {
                this.Selection.Changed -= selectionChangedHandler;

                valid = (this.Model as TreeStore).Remove(ref iter);

                this.Selection.Changed += selectionChangedHandler;

                if (!valid) {
                    EmitChannelSelected(null);
                    return;
                }

                this.Selection.SelectIter(iter);
            }
        }

        public bool NextUnread ()
        {
            TreeModel model;
            TreeIter  iter;

            if (this.Selection.GetSelected(out model, out iter)) {
                IChannel channel = (IChannel)model.GetValue(iter, 0);
                if (channel.NrOfUnreadItems > 0) {
                    return true;
                }

                if (!this.Model.IterNext(ref iter)) {
                    if (!this.Model.GetIterFirst(out iter)) {
                        return false;
                    }
                }
            } else {
                if (!this.Model.GetIterFirst(out iter)) {
                    return false;
                }
            }

            TreeIter startIter = iter;

            do {
                IChannel channel = model.GetValue(iter, 0) as IChannel;
                if (channel.NrOfUnreadItems > 0) {
                    this.Selection.SelectIter(iter);
                    ScrollToCell(this.Model.GetPath(iter), nameColumn,
                                 false, 0, 0);
                    return true;
                }

                if (!this.Model.IterNext(ref iter)) {
                    this.Model.GetIterFirst(out iter);
                }
            } while (!iter.Equals(startIter));

            return false;
        }

        private void EditChannelCb()
        {
            EmitEditChannelEvent(GetSelected ());
        }

        private void MarkAsReadCb ()
        {
            if (MarkChannelAsReadEvent != null) {
                MarkChannelAsReadEvent (GetSelected ());
            }
        }

        private void RemoveChannelCb ()
        {
            if (RemoveChannelEvent != null) {
                RemoveChannelEvent (GetSelected ());
            }
        }

        private void RefreshChannelCb ()
        {
            if (RefreshChannelEvent != null) {
                RefreshChannelEvent (GetSelected ());
            }
        }

        private int ChannelSort(TreeModel model, TreeIter ia, TreeIter ib)
        {
            IChannel a = Model.GetValue(ia, 0) as IChannel;
            IChannel b = Model.GetValue(ib, 0) as IChannel;

            return a.Name.CompareTo(b.Name);
        }

        protected override bool OnButtonPressEvent (EventButton bEvent)
        {
            switch (bEvent.Button) {
            case 1:
                if (bEvent.Type == EventType.TwoButtonPress) {
                    EditChannelCb();
                } else {
                    return base.OnButtonPressEvent (bEvent);
                }
                break;
            case 3:
                TreePath path;

                if (!GetPathAtPos ((int) bEvent.X, (int) bEvent.Y, out path)) {
                    return false;
                }

                this.Selection.SelectPath (path);

                popupMenu.Activate (bEvent);

//		popupMenu.Popup((uint)bEvent.XRoot, (uint)bEvent.YRoot, 
//				bEvent.Time);
                return false;
            }

            return false;
        }

        private void SelectionChanged(object obj, EventArgs args)
        {
            TreeSelection selection = (TreeSelection)obj;
            TreeIter      iter;
            TreeModel     model;
            IChannel      channel;

            if (!selection.GetSelected(out model, out iter)) {
                EmitChannelSelected(null);
                return;
            }

            channel = model.GetValue(iter, 0) as IChannel;
            if (channel != null) {
                EmitChannelSelected(channel);
            }
        }

        private void NamesCellDataFunc(TreeViewColumn col, CellRenderer cell, TreeModel model, TreeIter iter)
        {
            IChannel channel = model.GetValue(iter, 0) as IChannel;
            int weight = (int)Pango.Weight.Normal;

            (cell as CellRendererText).Text = channel.Name;
            if(channel.NrOfUnreadItems > 0){
                weight = (int)Pango.Weight.Bold;
            }

            (cell as CellRendererText).Ellipsize = Pango.EllipsizeMode.End;
            (cell as CellRendererText).Weight = weight;
        }

        private void ItemsCellDataFunc(TreeViewColumn col,
                                       CellRenderer   cell,
                                       TreeModel      model,
                                       TreeIter       iter)
        {
            IChannel channel = model.GetValue(iter, 0) as IChannel;
            int weight = (int)Pango.Weight.Normal;

            ((CellRendererText)cell).Text = channel.NrOfUnreadItems + "/" + channel.NrOfItems;
            if (channel.NrOfUnreadItems > 0) {
                weight = (int)Pango.Weight.Bold;
            }

            ((CellRendererText)cell).Xalign = 1.0f;
            ((CellRendererText)cell).Weight = weight;
        }

        private void EmitChannelSelected(IChannel channel)
        {
            if(LastChannel != null){
                foreach(Item item in LastChannel.Items){
                    item.Old = true;
                }
            }

            LastChannel = channel;

            if (ChannelSelectedEvent != null) {
                ChannelSelectedEvent(channel);
            }
        }

        // Used by ChannelUpdated
        private IChannel findChannel;
        private TreeIter foundIter;
        private bool ForeachFindChannel(TreeModel model, 
                                        TreePath  path,
                                        TreeIter  iter)
        {
            IChannel channel = model.GetValue(iter, 0) as IChannel;
            if (channel == findChannel) {
                foundIter = iter;
                return true;
            }

            return false;
        }

        private TreeIter FindChannel(IChannel channel)
        {
            findChannel = channel;
            foundIter = TreeIter.Zero;

            this.Model.Foreach(new TreeModelForeachFunc(ForeachFindChannel));

            return foundIter;
        }

        private void EmitEditChannelEvent(IChannel channel)
        {
            if (EditChannelEvent != null) {
                EditChannelEvent(channel);
            }
        }
    }

    class ChannelMenu {
        public delegate void MenuItemSelectedHandler();

        public event MenuItemSelectedHandler EditSelected;
        public event MenuItemSelectedHandler MarkAsReadSelected;
        public event MenuItemSelectedHandler RemoveSelected;
        public event MenuItemSelectedHandler RefreshSelected;

        public void Activate (Gdk.EventButton eb) 
        {
            Gtk.Menu popup_menu = new Gtk.Menu ();

            GtkUtil.AppendMenuItem (popup_menu, 
                                    Catalog.GetString ("_Mark as read"),
                                    new EventHandler (EmitMarkAsReadCb));
            GtkUtil.AppendMenuItem (popup_menu,
                                    Catalog.GetString ("_Refresh"),
                                    Stock.Refresh,
                                    new EventHandler (EmitRefreshChannelCb),
                                    true);
            GtkUtil.AppendMenuSeparator (popup_menu);
            GtkUtil.AppendMenuItem (popup_menu,
                                    Catalog.GetString ("_Edit"),
                                    Stock.Edit,
                                    new EventHandler (EmitEditCb),
                                    true);
            GtkUtil.AppendMenuItem (popup_menu,
                                    Catalog.GetString ("Remo_ve"),
                                    Stock.Remove,
                                    new EventHandler (EmitRemoveCb), true);
            popup_menu.Popup (null, null, null, 
                              eb.Button, eb.Time);
        }

        public void EmitMarkAsReadCb (object obj, EventArgs args)
        {
            if (MarkAsReadSelected != null) {
                MarkAsReadSelected ();
            }
        }

        public void EmitRefreshChannelCb (object obj, EventArgs args)
        {
            if (RefreshSelected != null) {
                RefreshSelected();
            }
        }

        public void EmitEditCb (object obj, EventArgs args) 
        {
            if (EditSelected != null) {
                EditSelected();
            }
        }

        public void EmitRemoveCb (object obj, EventArgs args)
        {
            if (RemoveSelected != null) {
                RemoveSelected();
            }
        }
    }
}

