//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using GConf;
using Gdk;
using Gtk;
using GLib;
using GtkSharp;
using System;
using System.Collections;

namespace Imendio.Blam {
    
    public class ItemList : Gtk.TreeView {

	public delegate void ItemSelectedHandler(Imendio.Blam.Item item);
	public event ItemSelectedHandler ItemSelected;

	private ItemView itemView;
	
    private TreeViewColumn titleColumn;
    private TreeViewColumn iconColumn;

    private Item lastItem;
    private uint lastTimeout;

	private IChannel channel;
	public IChannel CurrentChannel {
	    get {
		return channel;
	    }

	    set {
		channel = value;
		UpdateList();
	    }
	}
	
	public ItemList(ItemView itemView) 
	{
	    this.itemView = itemView;

        CellRendererPixbuf cell2 = new CellRendererPixbuf();
        iconColumn = new TreeViewColumn();
        iconColumn.PackStart(cell2, true);
        iconColumn.Sizing = TreeViewColumnSizing.GrowOnly;
        iconColumn.Expand = false;
        iconColumn.SetCellDataFunc(cell2,
                   new TreeCellDataFunc(IconCellDataFunc));

        AppendColumn(iconColumn);

	    titleColumn = new TreeViewColumn();
	    CellRendererText cell = new CellRendererText();

	    titleColumn.PackStart(cell, true);
	    titleColumn.SetCellDataFunc(cell, 
					new TreeCellDataFunc(NameCellDataFunc));

	    AppendColumn(titleColumn);

	    this.Selection.Changed += new EventHandler(SelectionChanged);
	    this.Model = new ListStore (typeof(Imendio.Blam.Item));
	    this.HeadersVisible = false;

        (Model as ListStore).DefaultSortFunc = CompareFunc;
        SetSortOrder(Conf.Get(Preference.REVERSE_ENTRIES, false));

	    Conf.AddNotify (Conf.GetFullKey(Preference.REVERSE_ENTRIES), new NotifyEventHandler (ConfNotifyHandler));

	}
	
	public void ItemAdded (Imendio.Blam.Item item)
	{
	    if (channel != null && channel == item.Channel) {
		((ListStore)this.Model).AppendValues(item);
	    }
	}

	public bool NextUnread()
	{
	    TreeModel model;
	    TreeIter  iter;
	    
	    if (this.Selection.GetSelected(out model, out iter)) { 
		if (!this.Model.IterNext(ref iter)) {
		    if (!((ListStore)this.Model).GetIterFirst(out iter)) {
			return false;
		    }
		}
	    } else {
		if (!((ListStore)this.Model).GetIterFirst(out iter)) {
		    return false;
		}
	    }

	    TreeIter startIter = iter;

	    do {
		Imendio.Blam.Item item = (Imendio.Blam.Item)model.GetValue(iter, 0);
		if (item.Unread) {
		    this.GrabFocus();
		    ScrollToCell(this.Model.GetPath(iter), titleColumn,
				 false, 0, 0);
		    
		    this.SetCursor(this.Model.GetPath(iter), 
				   titleColumn, false);
		    return true;
		}
		
		if (!this.Model.IterNext(ref iter)) {
		    this.Model.GetIterFirst(out iter);
		}
	    } while (!iter.Equals(startIter));

	    return false;
	}

	public bool IdleScrollCb ()
	{
	    TreeIter iter;
	    TreeIter lastIter = TreeIter.Zero;
	    TreePath path = null;

	    if (!this.Model.GetIterFirst (out iter)) {
		// No items 
		return false;
	    }

	    bool reverse = Conf.Get (Preference.REVERSE_ENTRIES, false);
	    
	    do {
		Item item = (Item) this.Model.GetValue (iter, 0);
		if (item.Unread) {
		    if (((ListStore)this.Model).IterIsValid (lastIter)) {
			path = this.Model.GetPath (lastIter);
		    } else {
			path = this.Model.GetPath (iter);
		    }
		    break;
		} 
		lastIter = iter;
	    } while (this.Model.IterNext (ref iter));
	   
	    if (path == null) { 
		if (reverse) {
		    path = TreePath.NewFirst ();
		} else {
		    if (!((ListStore)this.Model).IterIsValid (lastIter)) {
			return false;
		    }
		
		    path = this.Model.GetPath (lastIter);
		}
	    }

	    if (path != null) {
		ScrollToCell (path, titleColumn, true, 0, 0);
	    }
	    
	    return false;
	}

	public void UpdateList ()
	{
	    ((ListStore)this.Model).Clear();

	    if (this.channel == null) {
		return;
	    }

	    foreach (Imendio.Blam.Item item in this.channel.Items) {
		((ListStore)this.Model).AppendValues (item);
	    }

	    GLib.Timeout.Add (100, new GLib.TimeoutHandler (IdleScrollCb));
	}

	public void Update (Item item)
	{
	    TreeIter iter = FindItem (item);
	    
	    if (!iter.Equals (TreeIter.Zero)) {
		this.Model.EmitRowChanged (this.Model.GetPath(iter), iter);
	    }
	}

	public Item GetSelected ()
	{
	    TreeIter  iter;
	    TreeModel model;

	    if (!this.Selection.GetSelected (out model, out iter)) {
		return null;
	    }

	    return (Item) model.GetValue (iter, 0);
	}
	
	private void SelectionChanged (object obj, EventArgs args)
	{
	    TreeSelection selection = (TreeSelection) obj;
	    TreeIter iter;
	    TreeModel model;
	    Imendio.Blam.Item item;

	    if (!selection.GetSelected (out model, out iter)) {
		return;
	    }

	    item = (Imendio.Blam.Item) model.GetValue (iter, 0);
	    if (item != null) {
            EmitItemSelected (item);

            bool useTimeout = (bool) Conf.Get(Preference.MARK_ITEMS_READ, false);
            if(useTimeout){
                if(lastTimeout > 0)
                    GLib.Source.Remove(lastTimeout);

                uint readTimeout = (uint) Conf.Get(Preference.MARK_ITEMS_READ_TIMEOUT, 3000);
                lastItem = item;
                lastTimeout = GLib.Timeout.Add(readTimeout, new GLib.TimeoutHandler (SetToRead));
            } else {
                item.SetUnread(false, true);
            }
	    }
	}

    private bool SetToRead()
    {
        if(lastItem == GetSelected())
                lastItem.SetUnread(false, true);

        return false;
    }

    private int CompareFunc(TreeModel model, TreeIter a, TreeIter b)
    {
        Item ia = Model.GetValue(a, 0) as Item;
        Item ib = Model.GetValue(b, 0) as Item;

        return ia.PubDate.CompareTo(ib.PubDate);
    }

	protected override bool OnKeyPressEvent (EventKey kEvent)
	{
	    switch (kEvent.Key) {
	    case Gdk.Key.space:
	    case Gdk.Key.Page_Up:
	    case Gdk.Key.Page_Down:
		itemView.Widget.ProcessEvent (kEvent);
		return false;
		// return itemView.OnKeyPressEvent (kEvent);
	    default:
		return base.OnKeyPressEvent (kEvent);
	    }
	}

	private void NameCellDataFunc (TreeViewColumn col,
				       CellRenderer   cell,
				       TreeModel      model,
				       TreeIter       iter)
	{
	    Imendio.Blam.Item item = (Imendio.Blam.Item)model.GetValue(iter, 0);
	    
	    int weight = (int)Pango.Weight.Normal;

	    if (item.Unread) {
		weight = (int) Pango.Weight.Bold;
	    }

        ((CellRendererText)cell).Text = item.Title;
        ((CellRendererText)cell).Weight = weight;
        ((CellRendererText)cell).Ellipsize = Pango.EllipsizeMode.End;
	}

    private void IconCellDataFunc(TreeViewColumn col,
                       CellRenderer   cell,
				       TreeModel      model,
				       TreeIter       iter)
    {
        Item item = model.GetValue(iter, 0) as Imendio.Blam.Item;
        string icon = null;

        if(item.Unread == true){
            icon = Gnome.Stock.BookRed;
        } else {
            icon = Gnome.Stock.BookOpen;
        }

        if(item.Old == true && item.Unread == true){
            icon = Gnome.Stock.BookGreen;
        }

        if((cell as CellRendererPixbuf).IconName != icon){
            (cell as CellRendererPixbuf).IconName = icon;
        }
    }
	
	private void EmitItemSelected (Imendio.Blam.Item item) 
	{
	    if (ItemSelected != null) {
		ItemSelected (item);
	    }
	}
	
	private void SetSortOrder (bool reverseEntries)
	{
	    SortType sortType = SortType.Ascending;

	    if (reverseEntries) {
		sortType = SortType.Descending;
	    }
	    
	    ((ListStore)this.Model).SetSortColumnId (-1, sortType);
	}

	private void ConfNotifyHandler (object sender, NotifyEventArgs args)
	{
	    if (args.Key == Conf.GetFullKey (Preference.REVERSE_ENTRIES)) {
		SetSortOrder ((bool) args.Value);
	    }
	}

	// Used by Updated
	private Item     findItem;
	private TreeIter foundIter;

	private bool ForeachFindItem (TreeModel model, 
				      TreePath  path,
				      TreeIter  iter)
	{
	    Item item = (Item) model.GetValue (iter, 0);
	    
	    if (item == findItem) {
		foundIter = iter;
		return true;
	    }

	    return false;
	}

	private TreeIter FindItem (Item item)
	{
	    findItem = item;
	    foundIter = TreeIter.Zero;

	    this.Model.Foreach (new TreeModelForeachFunc (ForeachFindItem));

	    return foundIter;
	}
    }
}

