//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// (C) 2008 Nuanti Ltd.
//

using GConf;
using Glade;
using Gdk;
using Gtk;
using GtkSharp;
using Gnome;
using Mono.Unix;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using WebKit;
#if DBUS_SHARP
using DBus;
using org.freedesktop.DBus;
#elseif NDESK_DBUS
using NDesk.DBus;
using org.freedesktop.DBus;
#endif

namespace Imendio.Blam {

	public enum TargetType {
		String,
		UriList,
		Channel
	};

#if NDESK_DBUS || DBUS_SHARP
    [Interface("org.gnome.feed.Reader")]
#endif
    public interface IFeedReader {
        bool Subscribe(string url);
        void SetOnline(bool value);
    }

#if NDESK_DBUS || DBUS_SHARP
    [Interface("org.gnome.Blam.Reader")]
#endif
    public interface IBlamReader {
        void ShowWindow();
    }

    public class Application : Program, IBlamReader, IFeedReader {

#if ENABLE_DBUS
        static string bus_name = "org.gnome.feed.Reader";
        static ObjectPath obj_path = new ObjectPath("/org/gnome/feed/Reader");
#endif

	[DllImport("libc")]
	private static extern int prctl(int option, byte [] arg2, ulong arg3, 
    	ulong arg4, ulong arg5);
        
	public static Application TheApp;

        [Widget] Gtk.Window         mainWindow = null;
        [Widget] Gtk.ScrolledWindow channelListSw = null;
        [Widget] Gtk.ScrolledWindow itemListSw = null;
        [Widget] Gtk.Paned          channelPaned = null;
        [Widget] Gtk.Paned          itemPaned = null;
        [Widget] Gtk.Label          blogNameLabel = null;
        [Widget] Gtk.Statusbar      statusbar = null;
        [Widget] Gtk.Label          channelsLabel = null;
        private string              channelsLabelText;

        [Widget] Gtk.MenuItem       refreshChannelMenuItem = null;
        [Widget] Gtk.MenuItem       markChannelAsReadMenuItem = null;
        [Widget] Gtk.MenuItem       removeChannelMenuItem = null;
        [Widget] Gtk.MenuItem       editChannelMenuItem = null;

        [Widget] Gtk.MenuItem       markEntryAsUnreadMenuItem = null;
        [Widget] Gtk.MenuItem       editEntryKeywordsMenuItem = null;
        [Widget] Gtk.MenuItem       nextUnreadMenuItem = null;

        [Widget] Gtk.MenuItem       printMenuItem = null;

        private Gtk.FileChooserDialog   exportFileDialog;

        private ChannelList channelList;
        private ItemList    itemList;
        private ItemView    itemView;

        private ThemeManager mThemeManager;

        private TrayIcon    trayIcon;

        public static string BaseDir;

        private AddChannelDialog  addChannelDialog;
        private ChannelDialog     channelDialog;
        private AddGroupDialog    addGroupDialog;
        private PreferencesDialog preferencesDialog;
        private OpmlDialog        opmlDialog;

        private ChannelCollection mCollection;

        private uint              mAutoRefreshId;

        Gnome.Client client = null;

        public static TargetEntry[] DragEntries = new TargetEntry[] {
            new TargetEntry("STRING", 0, (uint)TargetType.String),
            new TargetEntry("text/plain", 0, (uint)TargetType.String),
            new TargetEntry("text/uri-list", 0, (uint)TargetType.UriList)
        };

        public Gtk.Window Window {
            get {
                return(Gtk.Window) mainWindow;
            }
        }

        public ChannelCollection CCollection {
            get {
                return mCollection;
            }
        }

        public ItemList ItemList {
            get {
                return itemList;
            }
        }

        public ChannelList ChannelList {
            get {
                return channelList;
            }
        }

		public ThemeManager ThemeManager {
			get {
				return mThemeManager;
			}
		}

        public Application (string[] args, params object[] props) : base ("Blam", Defines.VERSION, Modules.UI, args, props)
        {
            SetupDBus();

            client = new Gnome.Client();
            client.Connect();
            client.SaveYourself += SaveMyself;
            client.Die += QuitActivated;
            client.RestartStyle = RestartStyle.IfRunning;

            Proxy.InitProxy ();

            Catalog.Init ("blam", Defines.GNOME_LOCALE_DIR);

            if (!File.Exists(BaseDir)) {
                Directory.CreateDirectory(Defines.APP_HOMEDIR);
            }

            mCollection = ChannelCollection.LoadFromFile (Defines.APP_HOMEDIR + "/collection.xml");

            mCollection.ChannelUpdated         += ChannelUpdatedCb;
            mCollection.ChannelAdded           += ChannelAddedCb;
			mCollection.ChannelGroupAdded      += ChannelGroupAddedCb;
            mCollection.ChannelRemoved         += ChannelRemovedCb;
            mCollection.ChannelRefreshStarted  += ChannelRefreshStartedCb;
            mCollection.ChannelRefreshFinished += ChannelRefreshFinishedCb;

            mThemeManager = new ThemeManager ();

            PrepareGUI();

            if(Conf.Get(Preference.AUTO_REFRESH, false) == true){
                StartStopAutoRefresh();
                ShowNextUpdateTime();
            } else {
                if(Conf.Get(Preference.REFRESH_AT_START, false) == true){
                    mCollection.RefreshAll();
                }
            }

            Conf.AddNotify (Conf.GetFullKey(Preference.AUTO_REFRESH), new NotifyEventHandler (ConfNotifyHandler));
        }

        private void PrepareGUI()
        {
            Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
                                                        "mainWindow", null);
            gladeXML.Autoconnect(this);

            channelList = new ChannelList(mCollection.Channels, mCollection.Groups);
            ((Container)channelListSw).Child = channelList;

            channelList.ChannelSelectedEvent   += ChannelSelected;
            channelList.EditChannelEvent       += EditChannelActivated;
            channelList.MarkChannelAsReadEvent += MarkChannelAsReadActivated;
            channelList.RemoveChannelEvent     += RemoveChannelActivated;
            channelList.RefreshChannelEvent    += RefreshChannelActivated;

            itemView = new ItemView ();

            Frame f = new Frame ();
            f.Shadow = ShadowType.In;
            f.Add (itemView);
            itemPaned.Add2 (f);
            f.Show ();
            itemView.OnUrl += OnUrl;
            //itemView.Widget.DomMouseClick += new Gecko.DomMouseClickHandler(OnButtonPressEvent);

            itemList = new ItemList(itemView);
            ((Container)itemListSw).Child = itemList;

            itemList.ItemSelected += ItemSelected;

            trayIcon = new TrayIcon (Catalog.GetString ("Blam News Reader"));
            trayIcon.ButtonPressEvent += TrayIconButtonPressCb;
            trayIcon.RefreshAllEvent += RefreshAllActivated;
            trayIcon.PreferencesEvent += PreferencesActivated;
            trayIcon.AboutEvent += AboutActivated;
            trayIcon.QuitEvent += QuitActivated;

            channelsLabelText = channelsLabel.Text;
            UpdateTotalNumberOfUnread ();

            printMenuItem.Sensitive = false;
            SensitizeChannelMenuItems(false);

            // Setup drag-n-drop
            Gtk.Drag.DestSet(mainWindow, DestDefaults.All,
                             DragEntries, DragAction.Copy | DragAction.Move);
            mainWindow.DragDataReceived += DragDataReceivedCb;

            RestoreWindowState();

            mainWindow.IconName = "blam";

            mainWindow.ShowAll ();

            bool ShowItemList = Conf.Get(Preference.SHOW_ITEM_LIST, true);
            if(ShowItemList){
                itemPaned.Child1.Visible = true;
            } else {
                itemPaned.Child1.Visible = false;
            }

            channelDialog = new ChannelDialog (this);
            addChannelDialog = new AddChannelDialog (this);
            addGroupDialog = new AddGroupDialog (this);
            preferencesDialog = new PreferencesDialog (this.Window);
            opmlDialog = new OpmlDialog (this.Window);
            opmlDialog.ChannelAdded += mCollection.Add;
            opmlDialog.ImportFinished += OpmlImportFinished;

        }

        public void ToggleItemList()
        {
            if(itemPaned.Child1.Visible){
                itemPaned.Child1.Visible = false;
                Conf.Set(Preference.SHOW_ITEM_LIST, false);
            } else {
                itemPaned.Child1.Visible = true;
                Conf.Set(Preference.SHOW_ITEM_LIST, true);
            }
        }

        // Exposed by D-Bus
        public void ShowWindow()
        {
            this.mainWindow.Present();
        }

        // Exposed by D-Bus
        public bool Subscribe(string url)
        {
            Channel channel = new Channel();
            channel.Url = url;
            
            CCollection.Add(channel);
            
            return true;
        }

        // Exposed by D-Bus
        public void SetOnline(bool value)
        {
          // This is here to match Liferea.
          // Not yet implemented.
        }

#if ENABLE_DBUS
        private void SetupDBus()
        {
            try{
                BusG.Init();
                Bus bus = Bus.Session;

                if (bus.RequestName(bus_name) == RequestNameReply.PrimaryOwner) {
                    bus.Register(obj_path, this);
                } else {
                    IBlamReader reader = bus.GetObject<IBlamReader>(bus_name, obj_path);
                    reader.ShowWindow();
                    Gdk.Global.NotifyStartupComplete ();
                    Environment.Exit(0);
                }

            } catch (Exception e) {
                Console.Error.WriteLine("Unable to start D-Bus interface: {0}", e.Message);
            }
        }

        private void StopDBus()
        {
            try {
                Bus bus = Bus.Session;
                bus.Unregister(obj_path);
                bus.ReleaseName(bus_name);
            } catch (Exception e) {
                Console.Error.WriteLine("Unable to stop D-Bus interface: {0}", e.Message);
            }
        }
#else
        private void SetupDBus ()
        {
            // Do nothing
        }

        private void StopDBus()
        {
            // Stub
        }
#endif

        private void ChannelSelected(IChannel channel)
        {
            if (channel == null) {
                SensitizeChannelMenuItems(false);
                return;
            }

            itemList.CurrentChannel = channel;

            blogNameLabel.Markup = "<b>" + channel.Name + "</b>";
            mainWindow.Title = "Blam - " + channel.Name;

            SensitizeChannelMenuItems(true);
        }

        private void SensitizeChannelMenuItems (bool sensitive)
        {
            refreshChannelMenuItem.Sensitive = sensitive;
            removeChannelMenuItem.Sensitive = sensitive;
            editChannelMenuItem.Sensitive = sensitive;
            markChannelAsReadMenuItem.Sensitive = sensitive;
            editEntryKeywordsMenuItem.Sensitive = sensitive;
        }

        private void ItemSelected(Imendio.Blam.Item item)
        {
            itemView.CurrentItem = item;
            printMenuItem.Sensitive = true;
        }

        public void MarkEntryAsUnreadActivated (object obj, EventArgs args)
        {
            Item item = itemList.GetSelected ();
            if (item == null) {
                return;
            }

            // Toggle unread status
            item.SetUnread (!item.Unread, false);
        }

        public void EditEntryKeywordsActivated (object obj, EventArgs args)
        {
            Item item = itemList.GetSelected ();
            if (item == null) {
                return;
            }

            // FIXME: Show edit keywords dialog
        }

        public void MenuChannelActivated (object obj, EventArgs args)
        {
            IChannel channel = channelList.GetSelected ();

            bool sensitive = true;
            if (channel == null) {
                sensitive = false;
            }

            SensitizeChannelMenuItems (sensitive);
        }

        public void MenuEntryActivated (object obj, EventArgs args)
        {
            Item item = itemList.GetSelected ();

            if (item == null) {
                markEntryAsUnreadMenuItem.Sensitive = false;
                return;
            }

            markEntryAsUnreadMenuItem.Sensitive = true;

            string str = "";
            if (!item.Unread) {
                str = Catalog.GetString ("_Mark as unread");
            } else {
                str = Catalog.GetString ("_Mark as read");
            }

            ((Label) markEntryAsUnreadMenuItem.Child).TextWithMnemonic = str;
        }

        public void NextUnreadActivated(object obj, EventArgs args)
        {
            if (channelList.NextUnread()) {
                itemList.NextUnread();
            }
        }

        public void MainWindowKeyPressed (object obj, KeyPressEventArgs args)
        {
            switch (args.Event.Key) {
            case (Gdk.Key.period):
            case (Gdk.Key.bracketright):
                // Couldn't figure out how to get this to the menu item itself.
                nextUnreadMenuItem.Activate ();
                break;
            case (Gdk.Key.Escape):
                mainWindow.Hide ();
                trayIcon.Show();
                break;
            case (Gdk.Key.I):
            case (Gdk.Key.i):
                ToggleItemList();
                break;
            }
        }

        public void PrintActivated(object obj, EventArgs args)
        {
            //if (!itemView.PageLoaded)
            //  return;

            itemView.Widget.ExecuteScript ("print();");
        }

        public void ImportOpmlActivated (object obj, EventArgs args)
        {
            opmlDialog.Show ();
        }

        private bool ShowFileExistsDialog (Gtk.Window parentWindow, string fileName)
        {
            string str = String.Format (Catalog.GetString ("File {0} already exists"), fileName);

            string msg = Catalog.GetString ("Do you want to overwrite the file?");

            Gtk.Dialog dialog = ConfirmationDialog.Create (parentWindow,
                                                           Catalog.GetString ("_Overwrite"),
                                                           str, msg);

            int result = dialog.Run ();
            dialog.Destroy ();

            switch (result) {
            case (int)ResponseType.Ok:
                return true;
            }

            return false;
        }

        public void ExportOpmlActivated (object obj, EventArgs args)
        {
            if (exportFileDialog == null) {
                exportFileDialog = new Gtk.FileChooserDialog (Catalog.GetString ("Export to..."),
                                      mainWindow, FileChooserAction.Save,
                                      Catalog.GetString("Cancel"), ResponseType.Cancel,
                                      Catalog.GetString("Save"), ResponseType.Ok);
                exportFileDialog.IconName = "blam";
                exportFileDialog.Modal = true;
                exportFileDialog.TransientFor = mainWindow;
            }

            bool finished = false;
            bool write = false;
            string fileName = "";

            while (!finished) {
                int result = exportFileDialog.Run ();

                switch (result) {
                case (int)ResponseType.Ok:
                    fileName = exportFileDialog.Filename;

                    if (!File.Exists (fileName)) {
                        write = true;
                        finished = true;
                    } else {
                        write = ShowFileExistsDialog (exportFileDialog, fileName);
                        if (write) {
                            finished = true;
                        }
                    }
                    break;
                case (int)ResponseType.Cancel:
                    finished = true;
                    break;
                }
            }

            exportFileDialog.Hide ();

            if (write) {
                OpmlWriter.Write (mCollection, fileName);
            }
        }

        private void OpmlImportFinished (string status)
        {
            uint contextId = statusbar.GetContextId("status");

            if (status != null) {
                statusbar.Push(contextId, status);
            }
        }

        public void DeleteEventTriggered(object obj, DeleteEventArgs args)
        {
            mainWindow.Hide(); // only exit if we choose File|Quit
            trayIcon.Show();
            args.RetVal = true;
        }
        
        public void SaveMyself(object obj, SaveYourselfArgs args)
        {
            string[] argv = {"blam"};

            client.SetRestartCommand(1, argv);
        }

        public void QuitActivated(object obj, EventArgs args)
        {
            StopDBus();
            SaveWindowState();
            mainWindow.Hide();
            mCollection.SaveToFile ();
            mCollection.StopAllThreads();

            Quit();
        }

        public void CopyActivated (object obj, EventArgs args)
        {
            itemView.Widget.CopyClipboard();
        }

        public void PreferencesActivated (object obj, EventArgs args)
        {
            preferencesDialog.Show ();
        }

        public void AboutActivated (object obj, EventArgs args)
        {
            new AboutDialog(mainWindow);
        }

        public void AddChannelActivated (object obj, EventArgs args)
        {
            addChannelDialog.Show ();
        }

        public void AddGroupActivated (object obj, EventArgs args)
        {
            addGroupDialog.Show();
        }

        public void EditChannelActivated (IChannel channel)
        {
            bool IsGroup = false;

            foreach(ChannelGroup group in mCollection.Groups){
                if(channel == group){
                    IsGroup = true;
                }
            }

            if (channel != null && !IsGroup) {
                channelDialog.Show (channel as Channel);
            }
        }

        public void EditChannelActivated(object obj, EventArgs args)
        {
            EditChannelActivated (channelList.GetSelected ());
        }

        public void MarkChannelAsReadActivated (IChannel channel)
        {
            if (channel.MarkAsRead ()) {
                CCollection.Update (channel);
            }
        }

        public void MarkChannelAsReadActivated (object obj, EventArgs args)
        {
            MarkChannelAsReadActivated (channelList.GetSelected ());
        }

        public void MarkAllAsReadActivated(object o, EventArgs args)
        {
            foreach(Channel channel in CCollection.Channels){
                MarkChannelAsReadActivated(channel);
         }
        }

        public void RemoveChannelActivated(IChannel channel)
        {
            if (channel != null) {
                RemoveChannelDialog.Show (mainWindow, mCollection, channel);
            }
        }

        public void RemoveChannelActivated(object obj, EventArgs args)
        {
            IChannel channel = channelList.GetSelected ();

            RemoveChannelActivated(channel);
        }

        public void RefreshChannelActivated(IChannel channel)
        {
            if (channel != null) {
                mCollection.Refresh (channel);
            }
        }

        public void RefreshChannelActivated(object obj, EventArgs args)
        {
            IChannel channel = channelList.GetSelected ();

            RefreshChannelActivated(channel);
        }

        public void RefreshAllActivated(object obj, EventArgs args)
        {
            /* First move the refresh back */
            StartStopAutoRefresh();
            /* And pretend a timeout occurred */
            TimeoutRefreshAll();
        }

        private void DragDataReceivedCb(object o, DragDataReceivedArgs args)
        {
            SelectionData d = args.SelectionData;

            if (d.Length < 0 && d.Format != 8){
                Gtk.Drag.Finish(args.Context, false, false, args.Time);
                return;
            }

            Gtk.Drag.Finish(args.Context, true, true, args.Time);

            UTF8Encoding encoding = new UTF8Encoding( );
            string text = encoding.GetString(d.Data);

            addChannelDialog.Show (text);
        }

        private void OnUrl (string url)
        {
            uint contextId = statusbar.GetContextId("on_url");

            statusbar.Pop(contextId);
            if (url != null) {
                statusbar.Push(contextId, url);
            }
        }

        private void ChannelAddedCb (IChannel channel)
        {
            channelList.Add (channel);
        }

		private void ChannelGroupAddedCb(IChannel group, IChannel chan)
		{
			channelList.AddToGroup(group, chan);
		}

        private void ChannelUpdatedCb (IChannel channel)
        {
            channelList.Updated (channel);
            UpdateTotalNumberOfUnread ();
        }

        private void ChannelRemovedCb (IChannel channel)
        {
            channelList.Remove (channel);
            UpdateTotalNumberOfUnread ();
        }

        private void ChannelRefreshStartedCb (IChannel channel)
        {
            uint contextId = statusbar.GetContextId("update-status");
            string statusString;

            statusString = String.Format (Catalog.GetString ("Refreshing: {0}"), channel.Name);

            statusbar.Push (contextId, statusString);
        }

        private void ChannelRefreshFinishedCb (IChannel channel)
        {
            uint contextId = statusbar.GetContextId("update-status");

            statusbar.Pop (contextId);
            channelList.Updated (channel);
            UpdateTotalNumberOfUnread ();

            if (channelList.GetSelected () == channel) {
                itemList.UpdateList ();
            }
        }

        private void ShowNextUpdateTime()
        {
            uint contextId = statusbar.GetContextId("update-time");

            statusbar.Pop(contextId);

            int update_time = Conf.Get(Preference.AUTO_REFRESH_RATE, 15);
            DateTime NextUpdate = DateTime.Now.AddMinutes(update_time);

            /* This is the time (hour:minute) when the next update will be run */
            String StatusString = String.Format(Catalog.GetString("Next update at {0}"), NextUpdate.ToShortTimeString());

            statusbar.Push(contextId, StatusString);
        }

        private bool TimeoutRefreshAll ()
        {
            ShowNextUpdateTime();
            mCollection.RefreshAll();

            /* Continue until source is removed */
            return true;
        }

        private void StartStopAutoRefresh ()
        {
            bool doAutoRefresh;

            if (mAutoRefreshId != 0) {
                GLib.Source.Remove (mAutoRefreshId);
                mAutoRefreshId = 0;
            }

            doAutoRefresh = Conf.Get (Preference.AUTO_REFRESH, false);
            if (!doAutoRefresh) {
                return;
            }

            // Check if any feeds require refresh every ten seconds
            mAutoRefreshId = GLib.Timeout.Add ((uint) Conf.Get(Preference.AUTO_REFRESH_RATE, 15) * 60 * 1000,
                                               new GLib.TimeoutHandler (TimeoutRefreshAll));
        }

        private void ConfNotifyHandler (object sender, NotifyEventArgs args)
        {
            if (args.Key == Conf.GetFullKey (Preference.AUTO_REFRESH)) {
                StartStopAutoRefresh ();
            }
        }

        private void TrayIconButtonPressCb (object o, EventArgs args)
        {
            /* If we're hidden, show and if we're in fg, hide. */
            if(mainWindow.HasToplevelFocus){
                mainWindow.Hide();
            } else {
                mainWindow.Present();
            }
        }

        private void RestoreWindowState()
        {
            int width, height;
            int position_x, position_y;
            string state;

            state = Conf.Get ("ui/main_window_state", "normal");

            width = Conf.Get ("ui/main_window_width", 600);
            height = Conf.Get ("ui/main_window_height", 400);

            mainWindow.Resize(width, height);

            position_x = Conf.Get("ui/main_window_position_x", -1);
            position_y    =    Conf.Get(   "ui/main_window_position_y",    -1);
            if (position_x >= 0 && position_y >= 0) {
                mainWindow.Move(position_x, position_y);
            }

            width = Conf.Get("ui/channel_list_width", 180);
            channelPaned.Position = width;

            height = Conf.Get("ui/item_list_height", 100);
            itemPaned.Position = height;

            if (state == "maximized") {
                mainWindow.Maximize ();
            }
        }

        private void SaveWindowState()
        {
            int height, width;
            int position_x, position_y;

            if (mainWindow.GdkWindow.State != Gdk.WindowState.Maximized) {
                Conf.Set ("ui/main_window_state", "normal");

                mainWindow.GetSize (out width, out height);

                Conf.Set ("ui/main_window_width", width);
                Conf.Set ("ui/main_window_height", height);

                Conf.Set ("ui/channel_list_width", channelPaned.Position);
                Conf.Set ("ui/item_list_height", itemPaned.Position);

                mainWindow.GetPosition (out position_x, out position_y);

                Conf.Set ("ui/main_window_position_x", position_x);
                Conf.Set ("ui/main_window_position_y", position_y);
            } else {
                Conf.Set ("ui/main_window_state", "maximized");
            }

            Conf.Sync ();
        }

        private void UpdateTotalNumberOfUnread ()
        {
            int nrOfUnread, nrOfNew;

            nrOfUnread = mCollection.NrOfUnreadItems;
            nrOfNew = mCollection.NrOfNewItems;

            channelsLabel.Markup = string.Format("<b>" +
                                                 channelsLabelText +
                                                 "</b>",
                                                 nrOfUnread);

            /* Total number of unread items */
            string str = string.Format (Catalog.GetPluralString ("{0} unread item", "{0} unread items", nrOfUnread),
                                             nrOfUnread);
            str += " ";
            /* Number of new (not-skipped-over) entries. Gets appended to previous string */
            str += string.Format(Catalog.GetPluralString("({0} new)", "({0} new)", nrOfNew), nrOfNew);
            trayIcon.Tooltip = str;
        }

	public static void SetProcessName(string name)
	{
	    if(prctl(15 /* PR_SET_NAME */, Encoding.ASCII.GetBytes(name + "\0"), 
        	0, 0, 0) != 0) {
        	throw new ApplicationException("Error setting process name: " + 
        	    Mono.Unix.Native.Stdlib.GetLastError());
	    }
	}

        public static void Main(string[] args)
        {
            try {
                SetProcessName("blam");
						} catch {
						}

            TheApp = new Application (args);
            TheApp.Run ();
            // new Application(args).Run();
        }
    }
}
