//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using Gdk;
using Glade;
using Gtk;
using Mono.Unix;
using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;

namespace Imendio.Blam {

    public class OpmlDialog {
	[Widget] Gtk.Dialog opmlDialog   = null;
	[Widget] Gtk.Image  dialogImage  = null;
	[Widget] Gtk.Entry  urlEntry     = null;
	[Widget] Gtk.Button importButton = null;

	private Gtk.FileChooserDialog fileDialog;

	private OpmlStatusDialog opmlStatusDialog;

	public event ChannelEventHandler   ChannelAdded;
	public event ActionFinishedHandler ImportFinished;

	Gtk.Window mParentWindow;

	public string Url {
	    get {
		return urlEntry.Text;
	    }
	    set {
		urlEntry.Text = value;
	    }
	}

	public OpmlDialog (Gtk.Window parentWindow)
	{
	    Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
							"opmlDialog",
							null);
	    gladeXML.Autoconnect(this);

	    mParentWindow = parentWindow;
	    opmlDialog.TransientFor = mParentWindow;
	    opmlDialog.IconName = "blam";

	    dialogImage.Pixbuf =
		Gdk.Pixbuf.LoadFromResource ("blam-add-news.png");
	    
	    Gtk.Drag.DestSet(opmlDialog, DestDefaults.All,
			     Application.DragEntries,
			     DragAction.Copy | DragAction.Move);
	   
	    opmlDialog.DragDataReceived += DragDataReceivedCb;

	    opmlStatusDialog = new OpmlStatusDialog (parentWindow);
	}

	public void Show ()
	{
	    urlEntry.Text = "";
	    opmlDialog.ShowAll ();
	}
	
	private void DragDataReceivedCb(object o, DragDataReceivedArgs args)
	{
	    SelectionData d = args.SelectionData;
	    
	    if (d.Length < 0 && d.Format != 8){
		Gtk.Drag.Finish(args.Context, false, false, args.Time);
		return;
	    }

	    UTF8Encoding encoding = new UTF8Encoding( );
	    string text = encoding.GetString(d.Data);
	   
	    Url = text;
	    
	    Gtk.Drag.Finish(args.Context, true, true, args.Time);
	}

	public void FileButtonClicked(object obj, EventArgs args)
	{
	    if (fileDialog == null) {
		fileDialog = new Gtk.FileChooserDialog (Catalog.GetString ("Select OPML file"),
                          this.opmlDialog, FileChooserAction.Open,
                          Catalog.GetString("Cancel"), ResponseType.Cancel,
                          Catalog.GetString("Open"), ResponseType.Ok);
		fileDialog.Modal = true;
	    }

	    int result = fileDialog.Run();
	    
	    switch (result) {
	    case (int)ResponseType.Ok:
		urlEntry.Text = fileDialog.Filename;
		break;
	    }

	    fileDialog.Hide();
	}

	private int nrChannels;

	private void ChannelReadCb (IChannel channel)
	{
	    ++nrChannels;

	    if (ChannelAdded != null) {
		ChannelAdded (channel);
	    }
	}

	private void ImportFinishedCb ()
	{
	    // FIXME: Signal this to application
	    opmlStatusDialog.Hide ();

	    if (ImportFinished != null) {
		ImportFinished (String.Format (Catalog.GetString ("Imported {0} channels from OPML file"), nrChannels));
	    }
	}

	private void ImportErrorCb (string errorMsg)
	{
	    opmlStatusDialog.Hide ();

	    ShowErrorDialog (Url, errorMsg);
	}

	public void ShowErrorDialog (string url, string errorMsg)
	{
	    string str = String.Format (Catalog.GetString ("Failed to import {0}"), url);
	    Gtk.Dialog diag = ErrorDialog.Create(mParentWindow, str, errorMsg);
	    diag.Run();
	    diag.Destroy();

	}
	
	public static string GetXmlExceptionString ()
	{
	    return Catalog.GetString ("Not a valid OPML file");
	}

	public static string GetFileNotFoundExceptionString ()
	{
	    return Catalog.GetString ("File not found");
	}

	public static string GetUriFormatExceptionString ()
	{
	    return Catalog.GetString ("Invalid file name");
	}
	
	public static string GetWebExceptionString ()
	{
	    return Catalog.GetString ("Could not find OPML file");
	}
	
	public static string GetUnknownExceptionString ()
	{
	    return Catalog.GetString ("Unknown error");
	}
	
	public void ImportButtonClicked (object obj, EventArgs args)
	{
	    string errorMsg = "";
	    
	    try {
		Uri uri = new Uri (Url);
		OpmlReader reader = new OpmlReader (uri);
		reader.ChannelRead += ChannelReadCb;
		reader.ImportFinished += ImportFinishedCb;
		reader.ErrorEvent += ImportErrorCb;

		nrChannels = 0;
		opmlDialog.Hide ();
                opmlStatusDialog.Show (Catalog.GetString ("Opening OPML file"));
                reader.ReadChannelsAsync ();
                return;
	    }
	    catch (XmlException) {
		errorMsg = GetXmlExceptionString ();
	    }
	    catch (FileNotFoundException) {
		errorMsg = GetFileNotFoundExceptionString ();
	    }
	    catch (UriFormatException) {
		errorMsg = GetUriFormatExceptionString ();
	    }
	    catch {
		errorMsg = GetUnknownExceptionString ();
	    }
	    
	    opmlDialog.Hide ();
	    ShowErrorDialog (Url, errorMsg);
	}

	public void CancelButtonClicked (object obj, EventArgs args)
	{
	    opmlDialog.Hide ();
	}

	public void UrlEntryChanged (object obj, EventArgs args)
	{
	    if (urlEntry.Text == "") {
		importButton.Sensitive = false;
	    } else {
		importButton.Sensitive = true;
	    }
	}

	public void UrlEntryActivated (object obj, EventArgs args)
	{
	    if (urlEntry.Text == "") {
		return;
	    }

	    importButton.Click ();
	}
	
	public class OpmlStatusDialog {
	    [Widget] Gtk.Dialog      opmlStatusDialog = null;
	    [Widget] Gtk.ProgressBar progressBar      = null;

	    private uint timeoutId;
	    
	    public OpmlStatusDialog (Gtk.Window parentWindow)
	    {
		Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
							    "opmlStatusDialog",
							    null);
		gladeXML.Autoconnect(this);
		opmlStatusDialog.TransientFor = parentWindow;
		opmlStatusDialog.IconName = "blam";
	    }

	    public void Show (string initialStatus)
	    {
		timeoutId = GLib.Timeout.Add (100, new GLib.TimeoutHandler (UpdateProgress));
		opmlStatusDialog.ShowAll ();
	    }

	    public void Hide ()
	    {
		opmlStatusDialog.Hide ();
		if (timeoutId > 0) {
		    GLib.Source.Remove (timeoutId);
		    timeoutId = 0;
		}
	    }

	    public void CancelButtonClicked (object obj, EventArgs args)
	    {
		Console.Error.WriteLine ("Cancel");
		opmlStatusDialog.Hide ();
	    }

	    private bool UpdateProgress ()
	    {
		progressBar.Pulse ();
		return true;
	    }
	}
    }


    public class OpmlReader {

	public delegate void ImportFinishedHandler ();
	public delegate void ErrorHandler (string errorMsg);
	
	public event ChannelEventHandler   ChannelRead;
	public event ImportFinishedHandler ImportFinished;
	public event ErrorHandler          ErrorEvent;

	private Uri           mUri;

	private string        mErrorMsg;

	private IList         mChannels;
	protected IList ReadList {
	    get {
		return mChannels;
	    }
	}
	
	public OpmlReader (Uri uri)
	{
	    mUri = uri;
	    mChannels = ArrayList.Synchronized (new ArrayList ());
	}

	public void ReadChannelsAsync ()
	{
	    Thread thread = new Thread (new ThreadStart (ReadThread));
	    thread.Start ();
	}

	private void ReadThread ()
	{
	    try {
		XmlTextReader reader = new XmlTextReader (mUri.ToString ());
	
		while (reader.Read ()) {
		    string nodeName = reader.Name;
		    string name;
		    string url;

		    // Ignore all other nodes
		    if (nodeName == "outline") {
			name = reader.GetAttribute ("text");

                        if (name == null) {
                            name = reader.GetAttribute("title");
                        }

			url = reader.GetAttribute ("xmlUrl");
                        
			if (name == null || url == null) {
			    continue;
			}

			Channel channel = new Channel (name, url);
			new MainloopEmitter (this.ChannelRead, channel).Emit ();
		    }
		}
		GLib.Idle.Add (new GLib.IdleHandler (EmitImportFinished));
		return;
	    }
	    catch (XmlException) {
		mErrorMsg = OpmlDialog.GetXmlExceptionString ();
	    }
	    catch (FileNotFoundException) {
		mErrorMsg = OpmlDialog.GetFileNotFoundExceptionString ();
	    }
	    catch (UriFormatException) {
		mErrorMsg = OpmlDialog.GetUriFormatExceptionString ();
	    }
	    catch (System.Net.WebException) {
		mErrorMsg = OpmlDialog.GetWebExceptionString ();
	    }
	    catch {
		mErrorMsg = OpmlDialog.GetUnknownExceptionString ();
	    }

	    GLib.Idle.Add (new GLib.IdleHandler (EmitErrorEvent));
	}

	private bool EmitImportFinished ()
	{
	    if (ImportFinished != null) {
		ImportFinished ();
	    }

	    return false;
	}

	private bool EmitErrorEvent ()
	{
	    if (ErrorEvent != null) {
		ErrorEvent (mErrorMsg);
	    }

	    return false;
	}
    }

    public class OpmlWriter {
	public static void Write (ChannelCollection collection, string fileName)
	{
	    XmlTextWriter xtw = new XmlTextWriter (fileName, null);
	    
	    xtw.Formatting = Formatting.Indented;
	    xtw.Indentation = 2;

	    xtw.WriteStartDocument ();
	    xtw.WriteStartElement ("opml");
	    xtw.WriteAttributeString ("version", "1.0");
	    
	    WriteHeader (xtw);

	    WriteBody (xtw, collection);

	    xtw.WriteEndElement ();

	    xtw.Flush ();
	    xtw.Close ();
	}

	private static void WriteHeader (XmlTextWriter xtw)
	{
	    xtw.WriteStartElement ("head");
	    
	    xtw.WriteElementString ("dateCreated", DateTime.Now.ToString ("r"));
	    
	    xtw.WriteEndElement ();
	}

	private static void WriteBody (XmlTextWriter xtw, ChannelCollection collection)
	{
	    xtw.WriteStartElement ("body");

	    foreach (Channel channel in collection.Channels) {
		xtw.WriteStartElement ("outline");
		xtw.WriteAttributeString ("text", channel.Name);
		xtw.WriteAttributeString ("xmlUrl", channel.Url);
		xtw.WriteEndElement ();
	    }
	    
	    xtw.WriteEndElement ();
	}
    }
}
