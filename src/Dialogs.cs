//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using Glade;
using Gtk;
using Gdk;
using Mono.Unix;
using System;

namespace Imendio.Blam {

    class AboutDialog {
        [Widget] Gtk.AboutDialog aboutDialog = null;

        public AboutDialog(Gtk.Window parent)
        {
            Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
                                                        "aboutDialog", null);
            gladeXML.Autoconnect(this);

            aboutDialog.TransientFor = parent;
            aboutDialog.Version = Defines.VERSION;
            aboutDialog.LogoIconName = "blam";
            aboutDialog.IconName = "blam";
            aboutDialog.Copyright = "Copyright 2004-2006 (c) Mikael Hallendal <micke@imendio.com>\n"
                + "Copyright 2006-2008 (c) Carlos Martín Nieto <carlos@cmartin.tk>";
            aboutDialog.Run();
            aboutDialog.Destroy();
        }
        
    }

    class AddGroupDialog
    {
        [Widget] Gtk.Dialog addGroupDialog = null;
        [Widget] Gtk.Entry groupName       = null;
        [Widget] Gtk.Button ok             = null;

        Application mApp = null;

        public AddGroupDialog(Application app)
        {
            mApp = app;

            Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
                                 "addGroupDialog", null);
            gladeXML.Autoconnect(this);
            addGroupDialog.TransientFor = app.Window;
            addGroupDialog.IconName = "blam";
        }

        public void Show()
        {
            groupName.Text = "";
            addGroupDialog.ShowAll();
        }

        public void OkButtonClicked(object obj, EventArgs args)
        {
            ChannelGroup group = new ChannelGroup();
            group.Name = groupName.Text;
            mApp.CCollection.Groups.Add(group);
            Gtk.TreeIter iter = (mApp.ChannelList.Model as TreeStore).AppendValues(group);
            group.Iter = iter;

            addGroupDialog.Hide();
        }

        public void CancelButtonClicked(object obj, EventArgs args)
        {
            addGroupDialog.Hide();
        }
    }

    class AddChannelDialog {
    [Widget] Gtk.Dialog addChannelDialog = null;
    [Widget] Gtk.Entry  urlEntry         = null;
    [Widget] Gtk.Entry	usernameEntry	 = null;
    [Widget] Gtk.Entry	passwordEntry	 = null;
    [Widget] Gtk.Image  dialogImage      = null;
    [Widget] Gtk.Button okButton         = null;

    private static AddChannelDialog mThis;
	
	private Application mApp;
	
	public AddChannelDialog (Application app)
	{
	    mApp = app;
	    
	    Glade.XML gladeXML = Glade.XML.FromAssembly ("blam.glade",
							 "addChannelDialog",
							 null);
	    gladeXML.Autoconnect (this);
	    addChannelDialog.TransientFor = mApp.Window;
        addChannelDialog.IconName = "blam";
	    
	    dialogImage.Pixbuf =
		Gdk.Pixbuf.LoadFromResource ("blam-add-news.png");
	}

    public static void ClipboardTextReceived(Clipboard clipboard, string text)
    {
        if((text == null) || ((!text.StartsWith("http://")) && (!text.StartsWith("https://")))
               || (text == string.Empty)){
            mThis.urlEntry.Text = "";
        } else {
            mThis.urlEntry.Text = text;
        }
    }

    public void Show ()
    {
        mThis = this;
        Clipboard clipboard = Clipboard.Get(Gdk.Atom.Intern("CLIPBOARD", true));
        clipboard.RequestText(ClipboardTextReceived);

	    usernameEntry.Text = "";
	    passwordEntry.Text = "";
	 
	    addChannelDialog.ShowAll ();
	}

    public void Show (string url)
    {
        urlEntry.Text = url;
        usernameEntry.Text = "";
        passwordEntry.Text = "";
	    
        addChannelDialog.ShowAll ();
    }

	public void CancelButtonClicked (object obj, EventArgs args)
	{
	    addChannelDialog.Hide();
	}
	
	public void OkButtonClicked (object obj, EventArgs args)
	{
	    Channel channel = new Channel ();
		
	    channel.Url = urlEntry.Text;
	    channel.http_username = usernameEntry.Text;
	    channel.http_password = passwordEntry.Text;

	    mApp.CCollection.Add (channel);

	    addChannelDialog.Hide ();
	}

	public void EntryChanged (object obj, EventArgs args)
	{
	    if (!urlEntry.Text.Equals("")) {
		okButton.Sensitive = true;
	    } else {
		okButton.Sensitive = false;
	    }
	}
	
	public void EntryActivated (object obj, EventArgs args)
	{
	    if (!urlEntry.Text.Equals("")) {
		okButton.Click();
	    }
	}
    }
    
    class RemoveChannelDialog {
	[Widget] Gtk.Dialog removeChannelDialog = null;
	[Widget] Gtk.Label  dialogTextLabel     = null;

	private ChannelCollection mCollection;

	private static RemoveChannelDialog removeDialog = null;
	
	private RemoveChannelDialog(Gtk.Window parent,
				    ChannelCollection collection)
	{
	    mCollection = collection;
	    Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
							"removeChannelDialog",
							null);
	    gladeXML.Autoconnect(this);
	    removeChannelDialog.TransientFor = parent;
	    removeChannelDialog.IconName = "blam";
	}

	public static void Show (Gtk.Window parent, ChannelCollection collection, IChannel channel)
	{
	    if (removeDialog == null) {
		removeDialog = new RemoveChannelDialog (parent, collection);
	    }

	    string name = "<b>" + channel.Name + "</b>";
	  
	    string str = String.Format (Catalog.GetString ("Do you want to remove the channel or group {0} from the channel list?"), name);

	    removeDialog.dialogTextLabel.Markup = str;
	    
	    int response = removeDialog.removeChannelDialog.Run ();
	    removeDialog.removeChannelDialog.Hide ();
	    
	    switch (response) {
	    case (int) ResponseType.Cancel:
		return;
	    case (int) ResponseType.Ok:
		removeDialog.mCollection.Remove (channel);
		return;
	    }
	}
    }
}

