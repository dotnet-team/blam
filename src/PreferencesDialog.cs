//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using GConf;
using Glade;
using Gtk;
using System;
using System.Collections;

namespace Imendio.Blam {
    public class Preference {
        public static string REVERSE_ENTRIES   = "behaviour/reverse_entries";
        public static string REFRESH_AT_START  = "behaviour/refresh_at_start";
        public static string AUTO_REFRESH      = "behaviour/auto_refresh";
        public static string AUTO_REFRESH_RATE = "behaviour/auto_refresh_rate";
        public static string IGNORE_SSL_ERR    = "behaviour/ignore_ssl_err";
        public static string BEHAVIOUR_PATH    = Conf.GetFullKey("behaviour");
        public static string FONT_PATH         = "/desktop/gnome/interface";
        public static string VARIABLE_FONT     = "/desktop/gnome/interface/font_name";
        public static string FIXED_FONT        = "/desktop/gnome/interface/monospace_font_name";
        public static string THEME             = "ui/theme";
        public static string SHOW_ITEM_LIST    = "ui/show_item_list";
        public static string MARK_ITEMS_READ_TIMEOUT = "behaviour/set_item_read_timeout";
        public static string MARK_ITEMS_READ    = "behaviour/set_item_read";
    }

    public class PreferencesDialog {
        [Widget] Gtk.Dialog      preferencesDialog         = null;
        [Widget] Gtk.Label       boldBehaviourLabel        = null;
        [Widget] Gtk.CheckButton reverseEntriesCheckButton = null;
        [Widget] Gtk.CheckButton autoRefreshCheckButton    = null;
        [Widget] Gtk.CheckButton ignoreSSLErr              = null;
        [Widget] Gtk.CheckButton RefreshOnStartup          = null;
        [Widget] Gtk.SpinButton  refreshRateSpinButton     = null;
        [Widget] Gtk.Label       boldThemeLabel            = null;
        [Widget] Gtk.ComboBox    themeComboBox             = null;
        [Widget] Gtk.CheckButton markItemsReadCheckButton  = null;
        [Widget] Gtk.SpinButton  markItemsReadSpinButton   = null;

		public PreferencesDialog (Gtk.Window parentWindow)
		{
			Glade.XML gladeXML = Glade.XML.FromAssembly ("blam.glade",
								"preferencesDialog",
								null);
			gladeXML.Autoconnect (this);
			preferencesDialog.TransientFor = parentWindow;
			preferencesDialog.IconName = "blam";

			boldBehaviourLabel.Markup = "<b>" + boldBehaviourLabel.Text + "</b>";
			bool reverseEntries = Conf.Get (Preference.REVERSE_ENTRIES, false);
			reverseEntriesCheckButton.Active = reverseEntries;

			bool autoRefresh = Conf.Get (Preference.AUTO_REFRESH, false);
			autoRefreshCheckButton.Active = autoRefresh;

			int refreshRate = Conf.Get (Preference.AUTO_REFRESH_RATE, 15);
			refreshRateSpinButton.Value = refreshRate;

            ignoreSSLErr.Active = Conf.Get(Preference.IGNORE_SSL_ERR, false);

            RefreshOnStartup.Active = Conf.Get(Preference.REFRESH_AT_START, false);

			boldThemeLabel.Markup = "<b>" + boldThemeLabel.Text + "</b>";

			themeComboBox.Changed += new EventHandler(ThemeSelectionChanged);

            bool markItemsRead = Conf.Get(Preference.MARK_ITEMS_READ, false);
            markItemsReadCheckButton.Active = markItemsRead;

            int markItemsReadTimeout = Conf.Get(Preference.MARK_ITEMS_READ_TIMEOUT, 3000) / 1000;
            markItemsReadSpinButton.Value = markItemsReadTimeout;

			Conf.AddNotify (Preference.BEHAVIOUR_PATH, new NotifyEventHandler (ConfNotifyHandler));
		}

		public void Show ()
		{
			/* This needs to be this late, otherwise mono will die. */
			fill_themes_combo(themeComboBox);
			preferencesDialog.ShowAll ();
		}

		public void CloseButtonClicked (object obj, EventArgs args)
		{
			preferencesDialog.Hide();
		}

		public void ReverseItemsCheckButtonToggled (object obj, EventArgs args)
		{
			Conf.Set (Preference.REVERSE_ENTRIES,
					  reverseEntriesCheckButton.Active);
		}

		public void AutoRefreshCheckButtonToggled (object obj, EventArgs args)
		{
			Conf.Set (Preference.AUTO_REFRESH, autoRefreshCheckButton.Active);
		}

		public void RefreshRateSpinButtonChanged (object obj, EventArgs args)
		{
			Conf.Set (Preference.AUTO_REFRESH_RATE,
					  (int) refreshRateSpinButton.Value);
		}

        public void MarkItemsReadCheckButtonToggled(object obj, EventArgs args)
        {
            Conf.Set(Preference.MARK_ITEMS_READ, markItemsReadCheckButton.Active);
        }

        public void MarkItemsReadSpinButtonChanged(object obj, EventArgs args)
        {
            int timeoutms = (int) markItemsReadSpinButton.Value * 1000;
            Conf.Set(Preference.MARK_ITEMS_READ_TIMEOUT, timeoutms);
        }

		public void IgnoreSSLErrToggled (object obj, EventArgs args)
		{
		    Conf.Set (Preference.IGNORE_SSL_ERR,
		               (bool) ignoreSSLErr.Active);
		}

        public void RefreshOnStartupToggled(object o, EventArgs args)
        {
            Conf.Set(Preference.REFRESH_AT_START, (bool)RefreshOnStartup.Active);
        }

		public void ThemeSelectionChanged (object obj, EventArgs args)
		{
			Gtk.ComboBox box = obj as Gtk.ComboBox;
			Conf.Set(Preference.THEME, Application.TheApp.ThemeManager.PathByName(box.ActiveText));
		}

		private void ConfNotifyHandler (object sender, NotifyEventArgs args)
		{
			if (args.Key == Conf.GetFullKey (Preference.REVERSE_ENTRIES)) {
				reverseEntriesCheckButton.Active = (bool) args.Value;
			} else if (args.Key == Conf.GetFullKey (Preference.AUTO_REFRESH)) {
				autoRefreshCheckButton.Active = (bool) args.Value;
			} else if (args.Key == Conf.GetFullKey (Preference.AUTO_REFRESH_RATE)) {
				refreshRateSpinButton.Value = (int) args.Value;
			} else if (args.Key == Conf.GetFullKey (Preference.IGNORE_SSL_ERR)) {
			    ignoreSSLErr.Active = (bool) args.Value;
			}
		}
		
		private void fill_themes_combo(Gtk.ComboBox box)
		{
			box.Clear();
			CellRendererText cell = new CellRendererText();
			box.PackStart(cell, false);
			box.AddAttribute(cell, "text", 0);
			ListStore store = new ListStore(typeof(string));
			box.Model = store;

			TreeIter iter;
			string cur_theme = Conf.Get(Preference.THEME, Defines.DEFAULT_THEME);
			cur_theme = cur_theme.Substring(cur_theme.LastIndexOf("/") + 1);
			IList themes = Application.TheApp.ThemeManager.GetThemeList();
			
			foreach(Theme t in themes){
				iter = store.AppendValues(t.Name);

				if(t.Name == cur_theme){
					box.SetActiveIter(iter);
				}
			}
		}
		
	}
}

