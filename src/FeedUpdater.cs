//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using RSS;
using Atom;
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.XPath;

//using System.Web;

namespace Imendio.Blam {
    class CertCheck : ICertificatePolicy
    {
        public CertCheck()
        {
        }

        public bool CheckValidationResult(ServicePoint sp,
            System.Security.Cryptography.X509Certificates.X509Certificate cert,
            WebRequest request, int problem)
        {
            bool ignore = Conf.Get(Preference.IGNORE_SSL_ERR, false);
            if(ignore){
                Console.Error.WriteLine("I was asked! allowed!");         
                return true;
            } else {
                Console.Error.WriteLine("not allowing");
                return false;
            }
        }
    }
    public class FeedUpdater {
    
    private static string feed_type(ref string feed)
    {
        string type = null;
        try {
        XPathDocument doc = new XPathDocument(new StringReader(feed));
        XPathNavigator nav = doc.CreateNavigator();
        XmlNamespaceManager nsm = new XmlNamespaceManager(nav.NameTable);
        XPathExpression expr = nav.Compile("/atom03:feed|/atom10:feed|/rss10:RDF|/rss20:rss");
        
        nsm.AddNamespace("atom10", "http://www.w3.org/2005/Atom");
        nsm.AddNamespace("atom03", "http://purl.org/atom/ns#");
        nsm.AddNamespace("rss10",  "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        nsm.AddNamespace("rss20", "");
        expr.SetContext(nsm);
        
        XPathNodeIterator iter = nav.Select(expr);
        iter.MoveNext();
        
        if(iter.Current != null){
            switch(iter.Current.NamespaceURI){
                case "http://www.w3.org/2005/Atom":
                    type = "Atom 1.0";
                break;
                case "http://purl.org/atom/ns#":
                    type = "Atom 0.3";
                break;
                case "http://www.w3.org/1999/02/22-rdf-syntax-ns#":
                    type = "RSS 1.0";
                break;
                case "":
                    type = "RSS 2.0";
                break;
                default:
                    type = "unknown";
                break;
            }
        } else {
            type = "unknown";
        }
        } catch(Exception e){
                Console.Error.WriteLine("Error determining feed type: {0}", e.Message);
        }
        
        return type;
    }
    
    public static bool Update (Channel channel)
    {
        channel.LastRefreshed = DateTime.Now;

        bool updated = false;
        WebResponse res = null;
        bool remote = true; /* Wether the feed is local or remote. */
        StreamReader stream = null;
        string feed = null;

        if(channel.Url.StartsWith("/") || channel.Url.StartsWith("file:")){
            remote = false;
        }

        if(remote){
            res = GetRemoteChannelData(channel);

            if(res == null){
                return false; /* Feed hasn't changed or we couldn't connect. */
            }

            stream = new StreamReader(res.GetResponseStream());
            feed = stream.ReadToEnd();
            stream.Close();
        } else {
            feed = GetLocalChannel(channel);
            if(feed == string.Empty){
                return false;
            }
        }

        if(channel.Type == ""){
            string type = feed_type(ref feed);
            
            /* If we know for certain, assign it directly. */
            if(type == "Atom 0.3" || type == "Atom 1.0"){
                channel.Type = "Atom";
            } else if (type == "RSS 1.0" || type == "RSS 2.0"){
                channel.Type = "RSS";
            }

            /* If it's still unknown, we don't speak it. */
            if(channel.Type == ""){
                return false;
            }
        }

        try {
            
            if (channel.Type == "RSS") {
                updated = UpdateRssChannel(channel, ref feed);
            } else if (channel.Type == "Atom") {
                updated = UpdateAtomChannel(channel, ref feed);
            }

            if(remote){
                if (res.Headers.Get("Last-Modified") != null) {
                    channel.LastModified = res.Headers.Get("Last-Modified");
                }
                if (res.Headers.Get("ETag") != null) {
                    channel.ETag = res.Headers.Get("ETag");
                }

                res.Close();
            }

        } catch (Exception e) {
            Console.Error.WriteLine("Error whilst updating the feed {0}: {1}", channel.Name, e.Message);
        }

        return updated;
	}
	
    private static void set_credentials_from_uri(WebRequest req)
    {
        if (req.RequestUri.UserInfo != "") {
            int i = 0;
            string userInfo = req.RequestUri.UserInfo;
            string userName = "";

            while (i < userInfo.Length && userInfo[i] != ':')
                userName += Uri.HexUnescape(userInfo, ref i);

            if (i != userInfo.Length) {
                string password = "";

                i += 1;

                while (i < userInfo.Length && userInfo[i] != ':')
                    password += Uri.HexUnescape(userInfo, ref i);

                req.Credentials = new NetworkCredential(userName,
                                                        password);
            }
        }
    }

	private static WebResponse GetRemoteChannelData (Channel channel)
	{
	    HttpWebRequest req = null;
	    WebResponse res = null;

	    try {
	        req = (HttpWebRequest) WebRequest.Create(channel.Url);
	        
	        if(channel.http_username != ""){
                req.Credentials = new NetworkCredential(channel.http_username,
                                                        channel.http_password);
	        } else {
	            set_credentials_from_uri(req);
	        }

	        if (channel.LastModified != "") {
	            req.IfModifiedSince = DateTime.Parse(channel.LastModified);
	        }

	        if (channel.ETag != "") {
	            req.Headers.Add("If-None-Match", channel.ETag);
	        }

	        req.Timeout = 20000;
	        
	        ServicePointManager.CertificatePolicy = new CertCheck();

	        WebProxy proxy = Proxy.GetProxy();
	        if (proxy != null) {
	            req.Proxy = proxy;
	        }

	        try {
	            res = req.GetResponse();
	        } catch (WebException wE) {
	            switch (wE.Status) {
	            case WebExceptionStatus.ProtocolError:
		        if (((HttpWebResponse)wE.Response).StatusCode == 
			    HttpStatusCode.NotModified) {
		            //Console.Error.WriteLine("No changes to feed.");
		        }
		        if(((HttpWebResponse)wE.Response).StatusCode ==
		        HttpStatusCode.Unauthorized) {
		        	Console.Error.WriteLine("Unauthorised: " + wE.ToString());
		        }
		        break;
	            case WebExceptionStatus.Timeout:
		        //Console.Error.WriteLine("Timed out");
		        //Console.Error.WriteLine("Exception: " + wE.ToString());
		        break;
		        case WebExceptionStatus.TrustFailure:
		        Console.Error.WriteLine("TrustFailure: " + wE.ToString());
		        break;
	            default:
		        Console.Error.WriteLine("Exception: " + wE.ToString());
		        break;
	            }
	        }
	    } catch (Exception e) {
	        Console.Error.WriteLine("Big WebRequest error: {0}", e.Message);
	    }

	    return res;
	}

    private static string GetLocalChannel(Channel channel)
    {
        string fs_path = null;
        TextReader reader = null;
        int offset = -1;

        /*
         * First, we determine the format the feed was given in. We accept
         * several variants: "file:/u/feed.xml", "file:///u/feed.xml"
         * and "/u/feed.xml". We therefore need to find out how much to
         * strip from the beginning of the string to give us a path that
         * will be accepted by the file system.
         */

        if(channel.Url.StartsWith("/")){
            offset = 0;
        } else if(channel.Url.StartsWith("file:///")){
            offset = 7;
        } else if(channel.Url.StartsWith("file:/")){
            offset = 5;
        }

        fs_path = channel.Url.Substring(offset, channel.Url.Length - offset);
        try {
            reader = File.OpenText(fs_path);
        } catch(FileNotFoundException e){
            Console.Error.WriteLine("File {0} for URI {1} could not be found: {2}",
                                      fs_path, channel.Url, e.Message);
            return string.Empty;
        }

        return reader.ReadToEnd();
    }

    private static bool UpdateRssChannel(Channel channel, ref string feed_content)
    {
        bool ChanUpdated = false;
        RSSFeed feed = RSSFeed.Load(new StringReader(feed_content));

        channel.StartRefresh();

        /* FIXME: Figure out how we should support multiple channels in one feed. */

        if((channel.Name == "" || channel.Name == channel.Url) &&
                   feed.Channel[0].Title != null){
            channel.Name = HtmlUtils.StripHtml(feed.Channel[0].Title);
            ChanUpdated = true;
        }

        foreach (RSSItem item in feed.Channel[0].Item){
            string id = null;
            bool ItemUpdated = false;

            id = GenerateItemId(item);

            if(item.Title == null || item.Title == ""){
                if(item.Date != DateTime.MinValue){
                    item.Title = item.Date.ToString("d MMM yyyy");
                } else {
                    item.Title = feed.Channel[0].Title;
                }
            }

            ItemUpdated = channel.UpdateItem(id, item);

            if(ItemUpdated){
                ChanUpdated = true;
            }
        }

        channel.FinishRefresh();

        return ChanUpdated;
    }

	private static bool UpdateAtomChannel(Channel channel, ref string feed_content)
	{
	    bool channelUpdated = false;

	    try {
	        AtomFeed feed = AtomFeed.Load(new StringReader(feed_content));

	        if (feed != null) {
	            channel.StartRefresh();

	            if ((channel.Name == "" || channel.Name == channel.Url) && 
		        feed.Title.Text != null) {
		        channel.Name = HtmlUtils.StripHtml(feed.Title.Text.Trim());
		        channelUpdated = true;
	            }

	            foreach (AtomEntry entry in feed.Entry) {
		        string id;
		        bool   entryUpdated;

		        id = GenerateItemId(entry);
		        if (entry.Title.Text == "") {
		            if (!entry.Modified.Equals(DateTime.MinValue)) {
		                entry.Title.Text = 
		                    entry.Modified.ToString("d MMM yyyy");
		            } else {
		                entry.Title.Text = channel.Name;
		            }
		        }

		        entryUpdated = channel.UpdateItem (id, entry);
		        if (entryUpdated) {
		            channelUpdated = true;
		        }
	            }

	            channel.FinishRefresh();
	        }
	    } catch (Exception) {
	    }

        return channelUpdated;
	}

	public static string GenerateItemId (RSSItem item)
	{
	    if (item.Guid != null) {
		    return item.Guid;
	    } 
	    else if (item.Link != null) {
		    return item.Link;
	    } else {
		    return item.Title;
	    }
	}

	public static string GenerateItemId (AtomEntry entry)
	{
	    return entry.Id;
	}
    }
}
