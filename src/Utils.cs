//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using System;
using System.Runtime.InteropServices;
using Gtk;
using Pango;

using GLib;

namespace Imendio.Blam {
    public delegate bool ButtonPressHandler (IntPtr evnt);
    public delegate bool KeyPressHandler    (IntPtr evnt);
    public delegate int  SortFunc           (IntPtr a, IntPtr b);
    public delegate void DragDataHandler    (IntPtr context,
					     IntPtr data,
					     string text,
					     uint   time);

    // Taken from F-Spot
    class GtkUtil {
	public static void AppendMenuItem (Gtk.Menu menu, string l,
					   EventHandler e)
	{
	    AppendMenuItem (menu, l, e, true);
	}

	public static void AppendMenuItem (Gtk.Menu menu, string l,
					   EventHandler e, bool enabled)
        {
                Gtk.MenuItem i;
                Gtk.StockItem item = Gtk.StockItem.Zero;

                if (Gtk.StockManager.Lookup (l, ref item)) {
                        i = new Gtk.ImageMenuItem (l, new Gtk.AccelGroup ());
                } else {
                        i = new Gtk.MenuItem (l);
                }
                i.Activated += e;
                i.Sensitive = enabled;

                menu.Append (i);
                i.Show ();
	}

        public static void AppendMenuItem (Gtk.Menu menu, string label,
					   string image_name, EventHandler e,
					   bool enabled)
        {
                Gtk.ImageMenuItem i = new Gtk.ImageMenuItem (label);
                i.Activated += e;
                i.Sensitive = enabled;
                i.Image = new Gtk.Image (image_name, Gtk.IconSize.Menu);

                menu.Append (i);
                i.Show ();
        }

        public static void AppendMenuSeparator (Gtk.Menu menu)
        {
                Gtk.SeparatorMenuItem i = new Gtk.SeparatorMenuItem ();
                menu.Append (i);
                i.Show ();
        }
    }
}

	
