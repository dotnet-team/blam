// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// 

using RSS;
using Atom;
using System.Xml.Serialization;
using System;

namespace Imendio.Blam {
	public class Item {
		[XmlAttribute] public string   Id = "";
		[XmlAttribute] public bool     Unread = true;
		[XmlAttribute] public bool     Old = false;
		[XmlAttribute] public bool     Permanent = false;

		[XmlAttribute] public string   Title = "";
		[XmlAttribute] public string   Text = "";
		[XmlAttribute] public string   Link = "";
		[XmlAttribute] public DateTime PubDate;
		[XmlAttribute] public string   Author = "";

		[XmlAttribute] public string   keywords = "";

		[XmlIgnore]    public Channel  Channel;

		public string Keywords {
			get {
				return keywords;
			}
			set {
				if (value.Equals ("")) {
					this.Permanent = false;
				} else {
					this.Permanent = true;
				}

				this.keywords = value;
			}
		}

		public Item ()
		{
		}

		public Item (string id, RSSItem rssItem)
		{
			this.Id = id;
			this.Title = HtmlUtils.StripHtml(rssItem.Title.Trim ());
            
            if(rssItem.Description != null){
                this.Text = rssItem.Description.Trim();
            }

            if(rssItem.Content != null){
                this.Text = rssItem.Content.Trim();
            }

			if (rssItem.Link != null) {
				this.Link = rssItem.Link.Trim();
			//} else if (rssItem.Guid != null && !rssItem.Guid.PermaLink.IsFalse) {
			//	this.Link = rssItem.Guid.Name;
			} else {
				this.Link = "";
			}

			this.Author = rssItem.Author;

			this.PubDate = rssItem.Date;
		}

		public Item (string id, AtomEntry entry)
		{
			this.Id = id;
			this.Title = HtmlUtils.StripHtml(entry.Title.Text.Trim());

			if (entry.Author != null && entry.Author.Name != "") {
				this.Author = entry.Author.Name.Trim();
			}

			/* Content is optional in Atom feeds, or there may be multiple 
			 * contents of different media types.  Use the summary if there 
			 * is no content, otherwise use HTML if it's available.
			 */
			if (entry.Content == null || entry.Content.Length == 0) {
                Console.Error.WriteLine("no content, title= {0}", this.Title);
                if(entry.Summary.Text == null){
                    Console.Error.WriteLine("summary text null");
                    this.Text = "<p>There was no summary/content found for this entry. This is" + 
                        " most likely a bug.</p>";
                } else {
                    this.Text = entry.Summary.Text.Trim();
                }
			} else {
				foreach (AtomText content in entry.Content) {
					if (content.Type == "text/html" ||
                        content.Type == "html" ||
						content.Type == "application/xhtml+xml") {
						this.Text = content.Text.Trim();
						break;
					}
				}
			}

			/* Atom entries must have at least one link with a relationship 
			 * type of "alternate," but may have more.  Again, prefer HTML if 
			 * more than one is available.
			 */
			if (entry.Link.Length == 1) {
				this.Link = entry.Link[0].Url.Trim();
			} else {
                AtomLink link = entry.LinkByType("text/html");
                if(Link != null){
                    this.Link = link.Url;
                } else {
                    link = entry.LinkByType("application/xhtml+xml");
                    if(link != null){
                        this.Link = link.Url;
                    }
                }

			}

			this.PubDate = entry.Modified;
		}

		/* This is called in the middle of an refresh so the channel will be
		 * updated when refresh is done
		 */
		public bool Update (RSSItem rssItem)
		{
			if (this.Title != HtmlUtils.StripHtml(rssItem.Title.Trim ()) ) {
				this.Title = HtmlUtils.StripHtml(rssItem.Title.Trim ());
				this.SetUnread (true, true);
				return true;
			}
            
            if(rssItem.Description != null && this.Text != rssItem.Description.Trim()){
                this.Text = rssItem.Description.Trim();
                if(rssItem.Content != null && this.Text != rssItem.Content.Trim()){
                    this.Text = rssItem.Content.Trim();
                }
                    return true;
            }
			return false;
		}

		public bool Update (AtomEntry entry)
		{
			if (this.Title != HtmlUtils.StripHtml(entry.Title.Text.Trim())) {
				this.Title = HtmlUtils.StripHtml(entry.Title.Text.Trim());
				return true;
			}

			string entryText = "";
            if (entry.Content == null || entry.Content.Length == 0) {
                Console.Error.WriteLine("no content, title= {0}", this.Title);
                if(entry.Summary.Text == null){
                    Console.Error.WriteLine("summary text null");
                    this.Text = "<p>There was no summary/content found for this entry. This is" + 
                        " most likely a bug.</p>";
                } else {
                    this.Text = entry.Summary.Text.Trim();
                }
            } else {
                foreach (AtomText content in entry.Content) {
                    if (content.Type == "text/html" ||
                        content.Type == "html" ||
                        content.Type == "application/xhtml+xml") {
                        if(!this.Text.Equals(content.Text.Trim())){
                            this.Text = content.Text.Trim();
                        }
                        break;
                    }
                }
            }

			return false;
		}

		public void SetUnread (bool unread, bool inAllChannels) 
		{
			if (Unread != unread) {
				Unread = unread;
				Application.TheApp.CCollection.Update (this.Channel);
				Application.TheApp.ItemList.Update (this);

				if (unread != true && inAllChannels) {
					Application.TheApp.CCollection.MarkItemIdAsReadInAllChannels (this.Channel,
																				  this.Id);
				}
			}
		}
	}
}
