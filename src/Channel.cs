// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// 

using RSS;
using Atom;
using System.Collections;
using System;
using System.Net;
using System.Xml.Serialization;

namespace Imendio.Blam {

    public interface IChannel
    {
        int NrOfItems {get; }
        int NrOfUnreadItems {get; }
        string Name {get; set; }
        string Url {get; set; }
        ArrayList Items {get; set; }
        Gtk.TreeIter Iter {get; set; }
        bool MarkAsRead();
        Item GetItem(string id);
    }

    public class Channel : IChannel {
		[XmlAttribute("Name")] public string Int_Name = "";
		[XmlAttribute("Url")] public string Int_Url = "";

		// Used when updating the feed
		[XmlAttribute] public string LastModified = "";
		[XmlAttribute] public string ETag = "";
		[XmlAttribute] public string Type = "";
		[XmlAttribute] public string Keywords = "";
		[XmlAttribute] public DateTime LastRefreshed = new DateTime (0);
		
		// HTTP authentication options
		[XmlAttribute] public string http_username = "";
		[XmlAttribute] public string http_password = "";

        private Gtk.TreeIter mIter;

        public string Name {
            get {
                return Int_Name;
            }
            set {
                Int_Name = value;
            }
        }

        public string Url {
            get {
                return Int_Url;
            }
            set {
                Int_Url = value;
            }
        }

		public int NrOfItems {
			get {
				return mItems.Count;
			}
		}

		public int NrOfUnreadItems {
			get {
				int unread = 0;

				foreach (Item item in mItems) {
					if (item.Unread == true) {
						unread++;
					}
				}

				return unread;
			}
		}

        public int NrOfNewItems {
            get {
                int new_items = 0;

                foreach(Item item in mItems){
                    if(item.Unread && !item.Old){
                        ++new_items;
                    }
                }

                return new_items;
            }
        }

		ArrayList mItems;
		[XmlElement ("Item", typeof (Item))]
		public ArrayList Items {
			get {
				return mItems;
			}
			set {
				mItems = value;
			}
		}

		[XmlIgnore]
        public Gtk.TreeIter Iter {
            get {
                return mIter;
            }
            set {
                mIter = value;
            }
        }

		public Channel ()
		{
			mItems = new ArrayList ();
			mIter = new Gtk.TreeIter();
		}

		public Channel (string name, string url)
		{
			mItems = new ArrayList ();
			mIter = new Gtk.TreeIter();
			Name = name;
			Url = url;
		}

		public void Setup ()
		{
			foreach (Item item in mItems) {
				item.Channel = this;
			}
		}

		public Item GetItem (string id)
		{
			foreach (Item item in mItems) {
				if (item.Id == id) {
					return item;
				}
			}

			return null;
		}

		public bool MarkAsRead ()
		{
			bool updated = false;

			foreach (Item item in mItems) {
				if (item.Unread) {
					item.SetUnread (false, false);
					updated = true;
				}
			}

			return updated;
		}

		private ArrayList mUnupdatedItems;

		// Sets the channel in update mode. 
		public void StartRefresh ()
		{
			this.LastRefreshed = DateTime.Now;
			mUnupdatedItems = (ArrayList) mItems.Clone ();
		}

		// Removes any items not being part of the RSS feed any more
		public void FinishRefresh ()
		{
			// Remove old items
			foreach (Item item in mUnupdatedItems) {
				if (item.Permanent) {
					// Don't remove permanent items
					continue;
				}

				mItems.Remove (item);
			}
		}

		public bool UpdateItem (string id, RSSItem rssItem)
		{ 
			Item item = GetItem (id);

			if (item == null) {
				item = new Item (id, rssItem);
				item.Channel = this;
				item.Unread = !Application.TheApp.CCollection.CheckItemIdReadInAllChannels(id);

				mItems.Add (item);
				return true;
			} else {
				bool updated = item.Update (rssItem);
				mUnupdatedItems.Remove (item);
				return updated;
			}
		}

		public bool UpdateItem (string id, AtomEntry entry)
		{
			Item item = GetItem (id);

			if (item == null) {
				item = new Item (id, entry);
				item.Channel = this;

				mItems.Add (item);
				return true;
			} else {
				bool updated = item.Update (entry);
				mUnupdatedItems.Remove (item);
				return updated;
			}
		}

		/* Used to cross-mark as read */
		public void MarkItemIdAsRead (string id)
		{
			foreach (Item item in mItems) {
				if (item.Id.Equals (id)) {
					if (item.Unread) {
						item.Unread = false;
						Application.TheApp.CCollection.Update (this);
					}
					break;
				}
			}
		}

		public bool GetHasKeyword (string keyword)
		{
			if (Keywords.IndexOf (keyword) >= 0) {
				return true;
			}

			return false;
		}
	}


}
