//
// Author: 
//   Heath Harrelson <heath@pointedstick.net>
//
// (C) 2005
// 

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Imendio.Blam {

    public class HtmlUtils {
	// A list of al lthe HTML/XHTML tags.  This is the meat of the regex.
	static string taglist = @"xml|html|head|title|meta|style|script|" +
	                        @"base|link|noscript|body|iframe|noframes|a|p|" +
                                @"br|hr|h\d|div|span|ol|ul|menu|dir|li|dl|dt|dd|" +
                                @"pre|blockquote|center|ins|del|em|strong|dfn|" +
                                @"code|samp|kbd|var|cite|abbr|acronym|q|sub|sup|" +
                                @"tt|i|b|big|small|u|s|strike|basefont|font|" +
                                @"object|param|applet|img|map|area|form|input|" +
                                @"label|select|option|optgroup|textarea|fieldset|" +
                                @"legend|button|isindex|table|caption|thead|" +
	                        @"tfoot|tbody|colgroup|col|tr|th|td";

	static string tag_pattern =  @"<\s*?/?\s*?(\w+:)?(" + taglist + 
                                     @")\s*?.*?/?\s*?>";
	static Regex tags = new Regex(tag_pattern, RegexOptions.IgnoreCase | 
				      RegexOptions.Compiled | 
				      RegexOptions.Singleline);
	static Regex whitespace = new Regex(@"\s+", RegexOptions.Compiled);
	static Regex comment    = new Regex(@"<!--.*?-->", RegexOptions.Compiled |
					    RegexOptions.Singleline);

	public static string StripHtml (string buffer) 
	{
	    buffer = Unescape(buffer);
	    buffer = StripComments(buffer);
	    buffer = StripTags(buffer);
	    buffer = CollapseWhitespace(buffer);

	    return buffer;
	}

	public static string StripTags (string buffer) 
	{
	    return tags.Replace(buffer, "");
	}

	public static string StripComments (string buffer) 
	{
	    return comment.Replace(buffer, "");
	}

	public static string CollapseWhitespace (string buffer) 
	{
	    return whitespace.Replace(buffer, " ");
	}

	public static string Unescape (string buffer) 
	{
	    return HttpUtility.HtmlDecode(buffer);
	}

	public static string Escape (string buffer) 
	{
	    return HttpUtility.HtmlEncode(buffer);
	}

        public static string EncodeUnicode(string str)
        {
            StringBuilder output = new StringBuilder();
            foreach (char c in str)
            {
                if ((int)c > 128)
                {
                    output.Append("&#");
                    output.Append(((int)c).ToString());
                    output.Append(";");
                }
                else
                {
                    output.Append(c);
                }
            }
            return output.ToString();
        }

        public static string FixMarkup(string str)
        {
            string tStr = str.Trim().Replace("</p>", "").Replace("</P>", "");
            if (!tStr.StartsWith("<p>") && !tStr.StartsWith("<P>")) 
            {
                tStr = "<br/><br/>" + tStr;
            }
            return tStr.Replace("<p>", "<br/><br/>").Replace("<P>", "<br/><br/>");
        }
    }
}
