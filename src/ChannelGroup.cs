/*
 * Copyright (c) 2008 Carlos Martín Nieto <carlos@cmartin.tk>
 * 
 * This file is released under the GNU GPL v2 or later.
 */

using System;
using System.Collections;
using System.Xml.Serialization;

namespace Imendio.Blam
{
    [XmlType("group")]
    public class ChannelGroup : IChannel
    {
        [XmlAttribute("Name")] public string Int_Name = null;
        [XmlAttribute("Url")] public string Int_Url = null;
        [XmlElement("Channel", typeof(Channel))] public ArrayList Channels;
        private ArrayList dummy;
        private Gtk.TreeIter mIter;

        public ArrayList Items {
            get { /* FIXME: Cache this value. */
                ArrayList tmp = new ArrayList();
                foreach(Channel chan in Channels){
                    tmp.AddRange(chan.Items);
                }
                return tmp;
            }
            set {
                dummy = value;
            }
        }
        
        public int NrOfUnreadItems {
            get {
                int nr = 0;

                if(Channels.Count == 0)
                    return nr;

                foreach(IChannel channel in Channels){
                    nr += channel.NrOfUnreadItems;
                }

                return nr;
            }
        }

        public int NrOfNewItems {
            get {
                int nr = 0;

                if(Channels.Count == 0)
                    return nr;

                foreach(IChannel channel in Channels){
                    nr += channel.NrOfUnreadItems;
                }

                return nr;
            }
        }

        public int NrOfItems {
            get {
                int n = 0;

                if(Channels.Count == 0)
                    return n;

                foreach(IChannel channel in Channels){
                    n += channel.NrOfItems;
                }
                
                return n;
            }
        }
        public string Name {
            get {
                return Int_Name;
            }
            set {
                Int_Name = value;
            }
        }

        public string Url {
            get {
                return Int_Url;
            }
            set {
                Int_Url = value;
            }
        }

        [XmlIgnore]
        public Gtk.TreeIter Iter {
            get {
                return mIter;
            }
            set {
                mIter = value;
            }
        }

		public void Add(IChannel chan)
		{
			foreach(Channel ch in Channels){
				if(ch.Url == chan.Url)
					return;
			}

			if(chan.Name == null)
				chan.Name = chan.Url;

			Channels.Add(chan);
		}

        public ChannelGroup() : base()
        {
            if(Channels == null)
                Channels = new ArrayList();
        }

        public bool MarkAsRead()
        {
            bool ret = false;

            foreach(Channel chan in Channels){
                ret = chan.MarkAsRead();
            }
            return ret; /* FIXME: We shoudl probably do this some other way. */
        }

        public Item GetItem(string id)
        {
            Item item = null;
            foreach(IChannel ch in Channels){
                item = ch.GetItem(id);
                if(item != null){
                    return item;
                }
            }
            return null;
        }
    }
}
