//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// (C) 2008 Nuanti Ltd.
//

using GConf;
using Gtk;
using System;
using GtkSharp;
using WebKit;
using Mono.Unix;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Net;
using System.Text;

namespace Imendio.Blam {
    public class ItemView : Gtk.EventBox {
        private WebView webView;
#if ENABLE_FONTS
        private WebSettings webSettings;
#endif
	private Imendio.Blam.Item currentItem;
    private string baseDir = null;

	public bool PageLoaded;

        public event StringUpdatedHandler OnUrl;

	public Imendio.Blam.Item CurrentItem {
	    get {
		return currentItem;
	    }
	     
	    set {
		currentItem = value;
		Load ();
	    }
	}

        public WebView Widget {
            get {
                return webView;
            }
        }

	public ItemView () : base()
        {
            this.webView = new WebView ();
#if ENABLE_FONTS
            this.webSettings = new WebSettings ();
            webView.Settings = webSettings;
#endif
						ScrolledWindow sw = new ScrolledWindow ();
						sw.Add(webView);
            Add(sw);
	   
            Conf.AddNotify (Preference.FONT_PATH,
                                        new NotifyEventHandler (FontNotifyHandler));
            SetFonts ();

            ProxyUpdatedCb ();
            Proxy.Updated += ProxyUpdatedCb;

		webView.NavigationRequested += delegate (object sender, NavigationRequestedArgs args) {
        try {
            /* Allow our local files to be downloaded. Needed to load the theme */
            if(args.Request.Uri.StartsWith(baseDir)){
                    args.RetVal = NavigationResponse.Accept;
                    return;
            }
            Gnome.Url.Show(args.Request.Uri);
                    args.RetVal = NavigationResponse.Ignore;
        }
        catch (Exception e) {
            Console.Error.WriteLine("Couldn't show URL: " + args.Request.Uri + e.Message);
        }

				args.RetVal = NavigationResponse.Ignore;
		};

            webView.HoveringOverLink += delegate (object sender, HoveringOverLinkArgs args) {
                if (OnUrl != null)
                  OnUrl (args.Link);
            };

            webView.Show ();
            PageLoaded = false;
        }
        
        private void Load()
        {
			Theme theme = Application.TheApp.ThemeManager.CurrentTheme;

            string author  = (!"".Equals(currentItem.Author)) ? String.Format(Catalog.GetString("by {0}"), currentItem.Author) : "&nbsp;";
            string link    = Catalog.GetString("Show in browser");
            string pubdate = (!currentItem.PubDate.Equals(DateTime.MinValue)) ?
                currentItem.PubDate.ToString("D", System.Globalization.CultureInfo.CurrentUICulture) : "&nbsp;";
            string text    = HtmlUtils.EncodeUnicode(HtmlUtils.FixMarkup(currentItem.Text));
            string title   = HtmlUtils.Escape(currentItem.Title);
            string url     = currentItem.Link;
            baseDir        = "file://" + theme.Path;

            string[] replaces = {
                "author", author,
                "link", link,
                "pubdate", pubdate,
                "text", text,
                "title", title,
                "url", url,
                "localbase", baseDir
            };

			webView.LoadString(theme.Render(replaces), null, null, baseDir);
	}

        private void SetFonts ()
        {
            string varFont = Conf.Get (Preference.VARIABLE_FONT, "Sans 12");
            string fixedFont = Conf.Get (Preference.FIXED_FONT, "Mono 12");

            // Disabled for now since it's not clear that overriding the
            // default font settings makes sense.
#if ENABLE_FONTS
            Pango.FontDescription varDesc = Pango.FontDescription.FromString (varFont);
						webSettings.DefaultFontFamily = varDesc.Family;
						//webSettings.DefaultFontSize = varDesc.Size / 1024;

            Pango.FontDescription fixedDesc = Pango.FontDescription.FromString (fixedFont);
						webSettings.MonospaceFontFamily = fixedDesc.Family;
						//webSettings.MonospaceFontSize = fixedDesc.Size / 1024;
#endif
        }

        private void FontNotifyHandler (object sender, NotifyEventArgs args)
        {
            if (args.Key == Conf.GetFullKey (Preference.VARIABLE_FONT) ||
                args.Key == Conf.GetFullKey (Preference.FIXED_FONT)) {
                SetFonts ();
            }
        }

        private void ProxyUpdatedCb ()
        {
            //Utils.GeckoSetProxy (Proxy.UseProxy, Proxy.ProxyHost,
            //                     Proxy.ProxyPort);
        }
    }

#if ENABLE_FONTS
    class WebSettings : WebKit.WebSettings {
        public WebSettings() {}
        
        public string DefaultFontFamily {
            get { return (string)GetProperty("default-font-family").Val; }
            set { SetProperty("default-font-family", new GLib.Value(value)); }
        }

        public int DefaultFontSize {
            get { return (int)GetProperty("default-font-size").Val; }
            set { SetProperty("default-font-size", new GLib.Value(value)); }
        }

        public string MonospaceFontFamily {
            get { return (string)GetProperty("monospace-font-family").Val; }
            set { SetProperty("monospace-font-family", new GLib.Value(value)); }
        }
    }
#endif
}
