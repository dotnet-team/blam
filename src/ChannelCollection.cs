//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Imendio.Blam {

    public class ChannelCollection {

	public event ChannelEventHandler ChannelAdded;
	public event ChannelGroupEventHandler ChannelGroupAdded;
	public event ChannelEventHandler ChannelUpdated;
	public event ChannelEventHandler ChannelRemoved;
	
	public event ChannelEventHandler ChannelRefreshStarted;
	public event ChannelEventHandler ChannelRefreshFinished;

	public string FileName;

	private Queue mQueue;
	private IList mRunningList;

	private bool mDirty = false;
	private uint mTimeoutId = 0;
	private static uint WRITE_TIMEOUT = 5 * 60 * 1000; // Every 5 minutes
	
	static XmlSerializer serializer = new XmlSerializer (typeof (ChannelCollection));
	
	private ArrayList mChannels;
	[XmlElement ("Channel", typeof (Channel))]
	public ArrayList Channels {
	    get {
		return mChannels;
	    }
	    set {
		mChannels = value;
	    }
	}

	[XmlElement("Group", typeof(ChannelGroup))] public ArrayList Groups;

	public int NrOfUnreadItems {
	    get {
		int unread = 0;

		foreach (Channel channel in mChannels) {
		    unread += channel.NrOfUnreadItems;
		}

        foreach (ChannelGroup changrp in Groups) {
            unread += changrp.NrOfUnreadItems;
        }
		
		return unread;
	    }
	}

        public int NrOfNewItems {
            get {
                int new_items = 0;

                foreach(Channel channel in mChannels){
                    new_items += channel.NrOfNewItems;
                }

                foreach(ChannelGroup changrp in Groups){
                    new_items += changrp.NrOfNewItems;
                }

                return new_items;
            }
        }

	public ChannelCollection ()
	{
	    mQueue = Queue.Synchronized (new Queue ());
	    mRunningList = ArrayList.Synchronized (new ArrayList ());
	}

	public static ChannelCollection LoadFromFile (string file)
	{
	    ChannelCollection collection;

	    try {
		collection = RealLoadFromFile (file);
	    } catch {
		try {
		    collection = RealLoadFromFile (Defines.APP_DATADIR + "/collection.xml");
		} catch {
		    collection = new ChannelCollection ();
		    collection.mChannels = new ArrayList ();
		}
	    }

	    collection.FileName = file;

	    return collection;
	}

	private static ChannelCollection RealLoadFromFile (string file)
	{
	    ChannelCollection collection;
	    XmlTextReader reader = null;

	    try {
		reader = new XmlTextReader (file);
	    } catch (Exception) {
		reader = new XmlTextReader (GetTempFile (file));
	    }
	    
            collection = (ChannelCollection) serializer.Deserialize (reader);
            
	    reader.Close ();

            if (collection.Channels == null) {
                collection.mChannels = new ArrayList ();
            }

            foreach (Channel channel in collection.Channels) {
		channel.Setup ();
	    }

	    return collection;
	}
	
	public void SaveToFile ()
	{
	    lock (this) {
		if (!mDirty) {
		    return;
		}

		string tmpFile = GetTempFile (this.FileName);

		try {
		    Stream writer = new FileStream (tmpFile, FileMode.Create);
		    serializer.Serialize (writer, this);
		    writer.Close();
		} catch (Exception) {
		    Console.Error.WriteLine ("Failed to save to temporary file");
		    return;
		}

		// Move the file to the real one
		try {
		    File.Replace (tmpFile, this.FileName, this.FileName + ".bk");
		} catch (Exception e) {
		    Console.Error.WriteLine ("File replace error: " + e.Message);
		    return;
		}
		
		MarkAsDirty (false);
	    }
	}

	public void Add (IChannel channel)
	{
	    // Not the most efficient way of doing things :)
	    foreach (Channel ch in mChannels) {
		if (channel.Url == ch.Url) {
		    return;
		}
	    }

        if(channel.Name == null){
            channel.Name = channel.Url;
        }

        mChannels.Add (channel);
        Refresh (channel);

	    if (ChannelAdded != null) {
		ChannelAdded (channel);
	    }

        MarkAsDirty(true);
	}

	public void Add(ChannelGroup group, IChannel channel)
	{
		group.Add(channel);
		Refresh(channel);

		if(ChannelGroupAdded != null){
			ChannelGroupAdded(group, channel);
		}

		MarkAsDirty(true);
	}

	public void Update (IChannel channel)
	{
	    MarkAsDirty (true);

	    if (ChannelUpdated != null) {
		ChannelUpdated (channel);
	    }
	}

	public void Remove (IChannel channel)
	{
        /* Try to find out from which list we need to remove. */
        if(mChannels.Contains(channel)){
            mChannels.Remove (channel);
        } else if(Groups.Contains(channel)){
            Groups.Remove(channel);
        } else {
            /* It's not a first-level channel or group. Dig deeper. */
            foreach(ChannelGroup group in Groups){
                if(group.Channels.Contains(channel)){
                    group.Channels.Remove(channel);
                    break;
                }
            }
        }

	    if (ChannelRemoved != null) {
		ChannelRemoved (channel);
	    }

        MarkAsDirty(true);
	}

	public void Refresh (IChannel channel)
	{
	    mQueue.Enqueue (channel);
	    EmitChannelRefreshStarted (channel);

	    Thread thread = new Thread (new ThreadStart (UpdateThread));
	    mRunningList.Add (thread);

	    thread.Start ();
	}

	private void QueueChannelRefresh (IChannel channel)
	{
	    mQueue.Enqueue (channel);
	    EmitChannelRefreshStarted (channel);
	}

	private void StartRefreshThreads (int maxNrOfThreads)
	{
	    // Only start a maximum of five threads
	    for (int i = 0; i < 5 && i < maxNrOfThreads; ++i) { 
		Thread thread = new Thread (new ThreadStart (UpdateThread));
		mRunningList.Add (thread);
		thread.Start ();
	    }
	}

    public void StopAllThreads()
    {
        foreach(Thread t in mRunningList){
            t.Abort();
        }

        mRunningList.Clear();
        mQueue.Clear();
    }

	public void RefreshAll ()
	{
	    int nrOfChannels = 0;
	    
	    foreach (Channel channel in mChannels) {
		QueueChannelRefresh (channel);
		nrOfChannels++;
	    }

        foreach(ChannelGroup group in Groups){
            foreach(Channel channel in group.Channels){
                QueueChannelRefresh(channel);
                nrOfChannels++;
            }
        }

	    StartRefreshThreads (nrOfChannels);
	}
	
	/* Used to cross-mark as read */
	public void MarkItemIdAsReadInAllChannels (Channel channel, string id)
	{
	    foreach (Channel ch in mChannels) {
		if (ch != channel) {
		    ch.MarkItemIdAsRead (id);
		}
	    }
        foreach (ChannelGroup gr in Groups) {
            foreach(Channel chan in gr.Channels){
                if(chan != channel){
                    chan.MarkItemIdAsRead(id);
                }
            }
        }
	}

    public bool CheckItemIdReadInAllChannels(string id)
    {
        foreach(IChannel ch in mChannels){
            Item item = ch.GetItem(id);
            if(item != null){
                return true;
            }
        }
        return false;
    }

	private void UpdateThread ()
	{
	    while (true) {
		try {
		    IChannel tmp = (IChannel) mQueue.Dequeue ();
            Channel channel;

           if(tmp is ChannelGroup){
               mRunningList.Remove(Thread.CurrentThread);
               return;
            }

            channel = tmp as Channel;
            bool updated = FeedUpdater.Update (channel);

		    if (updated) {
			MarkAsDirty (true);
		    }	

		    new MainloopEmitter (this.ChannelRefreshFinished, channel).Emit ();
#if ENABLE_NOTIFY
            /* Make sure we have something to say and the user isn't watching us */
            if(mQueue.Count == 0 &&
                    NrOfUnreadItems != 0 && Application.TheApp.Window.IsActive == false){
                UnreadNotification.NotifyUnreadPosts(NrOfUnreadItems, NrOfNewItems);
            }
#endif
		} catch (InvalidOperationException) {
		    break;
		}
	    }

	    mRunningList.Remove (Thread.CurrentThread);
	}

	private void EmitChannelRefreshStarted (IChannel channel)
	{
	    if (ChannelRefreshStarted != null) {
		ChannelRefreshStarted (channel);
	    }
	}
	
	private bool WriteTimeoutHandler ()
	{
	    SaveToFile ();
	    
	    return false;
	}

	private void MarkAsDirty (bool dirty)
	{
	    lock (this) {
		if (dirty) {
		    if (mTimeoutId != 0) {
			GLib.Source.Remove (mTimeoutId);
		    } 
		
		    mTimeoutId = GLib.Timeout.Add (WRITE_TIMEOUT, new GLib.TimeoutHandler (WriteTimeoutHandler));
		} else {
		    if (mTimeoutId != 0) {
			GLib.Source.Remove (mTimeoutId);
			mTimeoutId = 0;
		    }
		}

		mDirty = dirty;
	    }
	}

	private static string GetTempFile (string fileName) 
	{
	    return fileName + ".tmp";
	}
    }
}

