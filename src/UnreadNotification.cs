using Mono.Unix;
using System;
using Notifications;

namespace Imendio.Blam
{
    public class UnreadNotification
    {

        static Notification note = new Notification();

        public UnreadNotification()
        {
            note = new Notification();
        }

        public static void NotifyUnreadPosts(int unread, int new_items)
        {
            /* Same as for the tray icon tooltip */
            /* Total number of unread items */
            string str = string.Format (Catalog.GetPluralString ("{0} unread item", "{0} unread items", unread),
                                             unread);
            str += " ";
            /* Number of new (not-skipped-over) entries. Gets appended to previous string */
            str += string.Format(Catalog.GetPluralString("({0} new)", "({0} new)", new_items), new_items);

            note.IconName = "blam";
            note.Summary = Catalog.GetString("Feeds refreshed");
            note.Body = str;
            note.Urgency = Urgency.Normal;
            note.Show();
        }
    }
}
