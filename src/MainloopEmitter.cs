//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB

using System.Collections;

namespace Imendio.Blam {

    public class MainloopEmitter {
	private ChannelEventHandler mHandler;
	private Channel             mChannel;

	private static IList mEmitters;
	
	public MainloopEmitter (ChannelEventHandler handler, Channel channel)
	{
	    if (mEmitters == null) {
		mEmitters = ArrayList.Synchronized (new ArrayList ());
	    }
	    
	    mHandler = handler;
	    mChannel = channel;

	    mEmitters.Add (this);
	}

	public void Emit ()
	{
	    GLib.Idle.Add (new GLib.IdleHandler (this.TimeoutEmit));
	}

	private bool TimeoutEmit ()
	{
	    if (mHandler != null) {
		mHandler (mChannel);
	    }

	    mEmitters.Remove (this);
            
	    return false;
	}
    }
}
