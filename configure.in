AC_INIT(README)
AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE(blam, 1.8.9)

AM_MAINTAINER_MODE

AC_PROG_INTLTOOL([0.25])

dnl Locate required programs
AC_PATH_PROG(GCONFTOOL, gconftool-2)
AM_GCONF_SOURCE_2

AC_PATH_PROG(MONO, mono)
if test "x$MONO" = "x" ; then
  AC_MSG_ERROR([Can't find "mono" in your PATH])
fi

AC_PATH_PROG(MCS, mcs)
AC_PATH_PROG(GMCS, gmcs)
if test "x$GMCS" != "x" ; then
   MCS=$GMCS
fi

if test "x$MCS" = "x" ; then
  AC_MSG_ERROR([Can't find "mcs" or "gmcs" in your PATH])
fi

AC_SUBST(PATH)
AC_SUBST(LD_LIBRARY_PATH)

AM_GCONF_SOURCE_2

dnl Find pkg-config
AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
if test "x$PKG_CONFIG" = "xno"; then
        AC_MSG_ERROR([You need to install pkg-config])
fi

dnl Find mono
MONO_REQUIRED_VERSION=1.1.17
GTKSHARP_REQUIRED_VERSION=2.8.2
GNOMESHARP_REQUIRED_VERSION=2.16.1
GCONF_REQUIRED_VERSION=2.4
WEBKITSHARP_REQUIRED_VERSION=0.2

PKG_CHECK_MODULES(BLAM,
		  gtk-sharp-2.0 >= $GTKSHARP_REQUIRED_VERSION
			webkit-sharp-1.0 >= $WEBKITSHARP_REQUIRED_VERSION
		  gconf-sharp-2.0 >= $GTKSHARP_REQUIRED_VERSION
		  glade-sharp-2.0 >= $GTKSHARP_REQUIRED_VERSION
		  gnome-sharp-2.0 >= $GNOMESHARP_REQUIRED_VERSION)

AC_SUBST(BLAM_LIBS)

#
# Use DBus# to export our remote control if available and the user wants it
#
AC_ARG_WITH([dbus],
	AC_HELP_STRING([--with-dbus], [Support D-Bus @<:@default=auto:>@]),
	[],
	with_dbus=auto
)
# In auto mode, prefer dbus-sharp
PKG_CHECK_EXISTS([dbus-sharp-1.0], DBUS_SHARP_SUPPORT=yes, DBUS_SHARP_SUPPORT=no)
PKG_CHECK_EXISTS([ndesk-dbus-1.0], NDESK_DBUS_SUPPORT=yes, NDESK_DBUS_SUPPORT=no)
if test "x$with_dbus" = "xauto"; then
   if test "$DBUS_SHARP_SUPPORT" = "xyes"; then
      ENABLE_DBUS=$DBUS_SHARP_SUPPORT
   else
      ENABLE_DBUS=$NDESK_DBUS_SUPPORT
   fi
fi
# If we found a package or the user told us
if test "x$with_dbus" = "xyes"; then
   if test "x$DBUS_SHARP_SUPPORT" = "xyes"; then
      PKG_CHECK_MODULES(DBUS_SHARP, dbus-sharp-glib-1.0, ENABLE_NDESK_DBUS="yes", ENABLE_NDESK_DBUS="no")
      AC_SUBST(DBUS_LIBS, "$DBUS_SHARP_LIBS")
   else
      PKG_CHECK_MODULES(DBUS_NDESK, ndesk-dbus-glib-1.0, ENABLE_DBUS_SHARP="yes", ENABLE_DBUS_SHARP="no")
      AC_SUBST(DBUS_LIBS, "$DBUS_NDESK_LIBS")
   fi
fi
# Tell the code what we're using
AM_CONDITIONAL(NDESK_DBUS, test "x$ENABLE_NDESK_DBUS" = "xyes")
AM_CONDITIONAL(DBUS_SHARP, test "x$ENABLE_DBUS_SHARP" = "xyes")

#
# Use libnotify is available
#
PKG_CHECK_MODULES(NOTIFY, notify-sharp, ENABLE_NOTIFY="yes", ENABLE_NOTIFY="no")
AM_CONDITIONAL(ENABLE_NOTIFY, test "$ENABLE_NOTIFY" = "yes")
AC_SUBST(NOTIFY_LIBS)


dnl Intl
ALL_LINGUAS="ar bg bs ca cs da de dz el en_CA en_GB es et eu fi fr ga gl gu he hr hu it lt lv ja nb ne nl oc pa pl pt_BR pt ru rw sk sl sq sr@Latn sr sv tr uk vi zh_CN zh_HK zh_TW"
GETTEXT_PACKAGE=blam
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Gettext package])
AM_GLIB_GNU_GETTEXT

CPPFLAGS=${_cppflags}
LDFLAGS=${_ldflags}

AC_OUTPUT([
Makefile
icons/Makefile
icons/16x16/Makefile
icons/22x22/Makefile
icons/24x24/Makefile
icons/32x32/Makefile
icons/48x48/Makefile
icons/scalable/Makefile
lib/Makefile
src/Makefile
po/Makefile.in
blam.desktop.in
themes/Makefile
themes/akregator/Makefile
themes/blam/Makefile
themes/pimped/Makefile
themes/planet/Makefile
])

echo
echo "Installing in prefix: $prefix"
echo 
echo "Enable D-Bus: $ENABLE_DBUS"
echo "Enable notifications: $ENABLE_NOTIFY"
echo
